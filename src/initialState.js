export const initialState = {
    init: {
        types: [],
        projects: [],
        countries: [],
        interests: [],
        sources: [],
        contacts: [],
        roles: [],
        actions: [],
        searchFields: []
    },
    customers: {
        list: [],
        active: 1,
        type: 1,
        page: 1,
        total: 0,
        perPage: 10,
        order_by: '',
        order_direction: 'asc',
        errors: {},
        modalActiveOrArchive: { show: false, action: 0, id: null },
        modalAddNote: { show: false, id: null },
        modalNotesList: { show: false, id: null }
    },
    notes: {
        id: null,
        data: [],
        page: 1,
        perPage: 10,
        total: 0
    },
    logsByUser: {
        list: [],
        actions: [],
        page: 1,
        total: 0,
        perPage: 10
    },
    logsByCustomer: {
        list: [],
        actions: [],
        page: 1,
        total: 0,
        perPage: 10
    },
    logsAll: {
        list: [],
        actions: [],
        page: 1,
        total: 0,
        perPage: 10
    },
    users: {
        list: [],
        page: 1,
        total: 0,
        perPage: 10,
        errors: {}
    },
    usersPartners: {
        list: [],
        errors: {}
    },
    interests: {
        list: [],
        errors: {}
    },
    projects: {
        list: [],
        errors: {}
    },
    reminders: {
        list: [],
        customersList: [],
        executed: 0,
        today: 0,
        page: 1,
        perPage: 10,
        total: 0,
        errors: {}
    },
    todayReminders: {
        list: [],
        count: 0,
        refresh: false
    },
    reminderModal: {
        show: false,
        id: 0
    },
    customer: {},
    user: {},
    userPartner: {
        user: {},
        children: {}
    },
    partnersCreated: 0,
    partnersCanCreate: 0,
    interest: {},
    project: {},
    reminder: {},
    authUser: {},
    currentProject: {
        created_at: null,
        deleted_at: null,
        id: 1,
        name_en: "Pustomyty",
        name_uk: "Пустомити",
        updated_at: null
    },
    redirect: false,
    translates: {},
    loading: false
}