import { initialState } from './initialState'
import { ACTIONS } from './actionsConstants'
import { combineReducers } from  'redux'
import { routerReducer } from 'react-router-redux'
import { localeReducer } from 'react-localize-redux'

export default combineReducers({
    routing: routerReducer,
    locale: localeReducer,
    reducer
})

function reducer(state = initialState, action) {
    // console.log('reducer', action)
    switch (action.type) {
        case ACTIONS.INIT.SUCCESS:
            return {
                ...state, init: {
                    contacts: action.payload.contacts,
                    searchFields: action.payload.searchFields,
                    countries: action.payload.countries,
                    projects: action.payload.projects,
                    interests: action.payload.interests,
                    sources: action.payload.sources,
                    actions: action.payload.actions,
                    roles: action.payload.roles,
                    types: action.payload.types,
                    lang: action.payload.lang ? action.payload.lang : initialState.init.lang
                }
            }

        case ACTIONS.AUTH.GET_USER.PENDING:
            return { ...state, loading: true }

        case ACTIONS.AUTH.GET_USER.SUCCESS:
            return { ...state, authUser: action.payload, loading: false }

        case ACTIONS.TRANSLATES.SUCCESS:
            return { ...state, translates: action.payload.translates }

        case ACTIONS.PROJECT.CHANGE:
            return { ...state, currentProject: action.payload }

        case ACTIONS.LANGUAGE.CHANGE:
            return { ...state, init: { ...state.init, lang: action.payload } }


        case ACTIONS.CUSTOMERS.CHANGE_TYPE:
            return { ...state, customers: { ...state.customers, type: action.payload } }

        case ACTIONS.CUSTOMERS.CHANGE_ACTIVE:
            return { ...state, customers: { ...state.customers, active: action.payload.active } }

        case ACTIONS.CUSTOMERS.SORT:
            return { ...state, customers: { ...state.customers, order_by: action.payload.order_by, order_direction: action.payload.order_direction } }

        case ACTIONS.CUSTOMERS.ACTIVE_MODAL.SHOW:
            return { ...state, customers: { ...state.customers, modalActiveOrArchive: { show: true, action: action.payload.action, id: action.payload.id } } }

        case ACTIONS.CUSTOMERS.ACTIVE_MODAL.HIDE:
            return { ...state, customers: { ...state.customers, modalActiveOrArchive: { show: false, action: 0, id: null }, errors: {} } }

        case ACTIONS.CUSTOMERS.NOTE_MODAL.SHOW:
            return { ...state, customers: { ...state.customers, modalAddNote: { show: true, id: action.payload.id } } }

        case ACTIONS.CUSTOMERS.NOTE_MODAL.HIDE:
            return { ...state, customers: { ...state.customers, modalAddNote: { show: false, id: null }, errors: {} } }

        case ACTIONS.CUSTOMERS.NOTES_LIST_MODAL.SHOW:
            return { ...state, customers: { ...state.customers, modalNotesList: { show: true, id: action.payload } } }

        case ACTIONS.CUSTOMERS.NOTES_LIST_MODAL.HIDE:
            return { ...state, customers: { ...state.customers, modalNotesList: { show: false, id: null } }, notes: {  data: [], page: 1, perPage: 10, total: 0} }

        case ACTIONS.CUSTOMERS.MOVE.SUCCESS:
            return { ...state, customers:  { ...state.customers, list: state.customers.list.filter(({ id }) => id !== action.payload.customer.id) } }

        case ACTIONS.CUSTOMERS.ACTIVE.SUCCESS:
            return { ...state, customers:  {
                ...state.customers,
                    list: state.customers.list.filter(({ id }) => id !== action.payload.customer.id),
                    modalActiveOrArchive: { show: false, action: 0, id: null },
                    errors: {}
            } }

        case ACTIONS.CUSTOMERS.ACTIVE.FAILED:
            return { ...state, customers:  { ...state.customers, errors: action.payload } }

        case ACTIONS.CUSTOMERS.ADD_NOTE.SUCCESS:
            return { ...state, customers:  { ...state.customers, modalAddNote: { show: false, id: null }, errors: {}} }

        case ACTIONS.CUSTOMERS.ADD_NOTE.FAILED:
            return { ...state, customers:  { ...state.customers, errors: action.payload } }

        case ACTIONS.CUSTOMERS.GET_LIST.PENDING:
            return { ...state, loading: true }

        case ACTIONS.CUSTOMERS.GET_LIST.SUCCESS:
            return {
                ...state, customers: {
                    list: action.payload.data,
                    page: action.payload.current_page,
                    perPage: action.payload.per_page,
                    total: action.payload.total,
                    active: state.customers.active,
                    type: state.customers.type,
                    order_by: state.customers.order_by,
                    order_direction: state.customers.order_direction,
                    modalActiveOrArchive: state.customers.modalActiveOrArchive,
                    modalAddNote: state.customers.modalAddNote,
                    modalNotesList: state.customers.modalNotesList
                },
                loading: false
            }

        case ACTIONS.CUSTOMERS.GET_ONE.PENDING:
            return { ...state, loading: true }

        case ACTIONS.CUSTOMERS.GET_ONE.SUCCESS:
            return { ...state, customer: action.payload.data, loading: false }

        case ACTIONS.CUSTOMERS.GET_ONE.FAILED:
            return { ...state, customers: { ...state.customers, errors: action.payload }, loading: false }

        case ACTIONS.CUSTOMERS.GET_NOTES_LIST.PENDING:
            return { ...state, loading: true }

        case ACTIONS.CUSTOMERS.GET_NOTES_LIST.SUCCESS:
            return {
                ...state, notes: {
                    data: action.payload.data,
                    page: action.payload.current_page,
                    perPage: action.payload.per_page,
                    total: action.payload.total
                },
                loading: false
            }


        case ACTIONS.LOGS.GET_LIST_BY_CUSTOMER.PENDING:
            return { ...state, loading: true }

        case ACTIONS.LOGS.GET_LIST_BY_CUSTOMER.SUCCESS:
            return {
                ...state,
                logsByCustomer: {
                    list: action.payload.log.data,
                    page: action.payload.log.current_page,
                    perPage: action.payload.log.per_page,
                    total: action.payload.log.total,
                    actions: action.payload.actions
                },
                customer: action.payload.customer,
                loading: false
            }

        case ACTIONS.LOGS.GET_LIST_BY_USER.PENDING:
            return { ...state, loading: true }

        case ACTIONS.LOGS.GET_LIST_BY_USER.SUCCESS:
            return {
                ...state,
                logsByUser: {
                    list: action.payload.log.data,
                    page: action.payload.log.current_page,
                    perPage: action.payload.log.per_page,
                    total: action.payload.log.total,
                    actions: action.payload.actions
                },
                user: action.payload.user,
                loading: false
            }

        case ACTIONS.LOGS.GET_ALL.PENDING:
            return { ...state, loading: true }

        case ACTIONS.LOGS.GET_ALL.SUCCESS:
            return {
                ...state,
                logsAll: {
                    list: action.payload.log.data,
                    page: action.payload.log.current_page,
                    perPage: action.payload.log.per_page,
                    total: action.payload.log.total,
                    actions: action.payload.actions
                },
                loading: false
            }

        case ACTIONS.CUSTOMERS.CREATE.SUCCESS:
            return { ...state, redirect: true }

        case ACTIONS.CUSTOMERS.CREATE.FAILED:
            return { ...state, customers: { ...state.customers, errors: action.payload.errors } }

        case ACTIONS.CUSTOMERS.UPDATE.SUCCESS:
            return { ...state, redirect: true }

        case ACTIONS.CUSTOMERS.UPDATE.FAILED:
            return { ...state, customers: { ...state.customers, errors: action.payload.errors } }


        case ACTIONS.USERS.GET_LIST.PENDING:
            return { ...state, loading: true }

        case ACTIONS.USERS.GET_LIST.SUCCESS:
            return {
                ...state, users: {
                    list: action.payload.data,
                    page: action.payload.current_page,
                    perPage: action.payload.per_page,
                    total: action.payload.total,
                },
                loading: false
            }

        case ACTIONS.USERS.GET_ONE.SUCCESS:
            return { ...state, user: action.payload.user }

        case ACTIONS.USERS.CREATE.SUCCESS:
            return { ...state, redirect: true }

        case ACTIONS.USERS.CREATE.FAILED:
            return { ...state, users: { ...state.users, errors: action.payload.errors } }

        case ACTIONS.USERS.UPDATE.SUCCESS:
            return { ...state, redirect: true }

        case ACTIONS.USERS.UPDATE.FAILED:
            return { ...state, users: { ...state.users, errors: action.payload.errors } }

        case ACTIONS.USERS.DELETE.SUCCESS:
            return { ...state, users: { ...state.users, list: state.users.list.filter(({ id }) => id !== action.payload.user.id) } }


        case ACTIONS.USERS_PARTNERS.GET_LIST.PENDING:
            return { ...state, loading: true }

        case ACTIONS.USERS_PARTNERS.GET_LIST.SUCCESS:
            return {
                ...state, usersPartners: {
                    list: action.payload,
                },
                loading: false
            }

        case ACTIONS.USERS_PARTNERS.GET_ONE.PENDING:
            return { ...state, loading: true }

        case ACTIONS.USERS_PARTNERS.GET_ONE.SUCCESS:
            return {
                ...state, userPartner: {
                    user: action.payload.user,
                    children: action.payload.children
                },
                loading: false
            }

        case ACTIONS.USERS_PARTNERS.GET_COUNT.SUCCESS:
            return {
                ...state, partnersCreated: action.payload.created, partnersCanCreate: action.payload.canCreate,
            }

        case ACTIONS.USERS_PARTNERS.CREATE.SUCCESS:
            return { ...state, redirect: true }

        case ACTIONS.USERS_PARTNERS.CREATE.FAILED:
            return { ...state, usersPartners: { ...state.users, errors: action.payload.errors } }

        case ACTIONS.USERS_PARTNERS.UPDATE.SUCCESS:
            return { ...state, redirect: true }

        case ACTIONS.USERS_PARTNERS.UPDATE.FAILED:
            return { ...state, users: { ...state.users, errors: action.payload.errors } }

        case ACTIONS.USERS_PARTNERS.DELETE.SUCCESS:
            return { ...state, usersPartners: { ...state.usersPartners, list: state.usersPartners.list.filter(({ id }) => id !== action.payload.user.id) } }


        case ACTIONS.INTERESTS.GET_LIST.PENDING:
            return { ...state, loading: true }

        case ACTIONS.INTERESTS.GET_LIST.SUCCESS:
            return { ...state, interests: { list: action.payload, errors: {} }, loading: false }

        case ACTIONS.INTERESTS.GET_ONE.SUCCESS:
            return { ...state, interest: action.payload.interest }

        case ACTIONS.INTERESTS.CREATE.SUCCESS:
            return { ...state, redirect: true }

        case ACTIONS.INTERESTS.CREATE.FAILED:
            return { ...state, interests: { ...state.interests, errors: action.payload.errors } }

        case ACTIONS.INTERESTS.UPDATE.SUCCESS:
            return { ...state, redirect: true }

        case ACTIONS.INTERESTS.UPDATE.FAILED:
            return { ...state, interests: { ...state.interests, errors: action.payload.errors } }

        case ACTIONS.INTERESTS.DELETE.SUCCESS:
            console.log(action.payload)
            return { ...state, interests: { ...state.interests, list: state.interests.list.filter(({ id }) => id !== action.payload.interest.id) } }


        case ACTIONS.PROJECTS.GET_LIST.PENDING:
            return { ...state, loading: true }

        case ACTIONS.PROJECTS.GET_LIST.SUCCESS:
            return { ...state, projects: { list: action.payload }, loading: false }

        case ACTIONS.PROJECTS.GET_ONE.SUCCESS:
            return { ...state, project: action.payload.project }

        case ACTIONS.PROJECTS.CREATE.SUCCESS:
            return { ...state, redirect: true }

        case ACTIONS.PROJECTS.CREATE.FAILED:
            return { ...state, projects: { ...state.projects, errors: action.payload.errors } }

        case ACTIONS.PROJECTS.UPDATE.SUCCESS:
            return { ...state, redirect: true }

        case ACTIONS.PROJECTS.UPDATE.FAILED:
            return { ...state, projects: { ...state.projects, errors: action.payload.errors } }

        case ACTIONS.PROJECTS.DELETE.SUCCESS:
            return { ...state, projects: { ...state.projects, list: state.projects.list.filter(({ id }) => id !== action.payload.project.id) } }


        case ACTIONS.REMINDERS.GET_LIST.PENDING:
            return { ...state, loading: true }

        case ACTIONS.REMINDERS.GET_LIST.SUCCESS:
            return {
                ...state, reminders: {
                    list: action.payload.data,
                    page: action.payload.current_page,
                    perPage: action.payload.per_page,
                    total: action.payload.total,
                    executed: state.reminders.executed,
                    today: state.reminders.today,
                }, loading: false
            }

        case ACTIONS.REMINDERS.GET_TODAY_LIST.SUCCESS:
            return {
                ...state, todayReminders: {
                    list: action.payload.list,
                    count: action.payload.count,
                    refresh: false
                }, loading: false
            }

        case ACTIONS.REMINDERS.GET_LIST_CUSTOMERS.PENDING:
            return { ...state, loading: true }

        case ACTIONS.REMINDERS.GET_LIST_CUSTOMERS.SUCCESS:
            return { ...state, reminders: { customersList: action.payload }, loading: false }

        case ACTIONS.REMINDERS.EDIT.PENDING:
            return { ...state, loading: true }

        case ACTIONS.REMINDERS.EDIT.SUCCESS:
            return { ...state, reminders: { customersList: action.payload.customers }, reminder: action.payload.reminder, loading: false }

        case ACTIONS.REMINDERS.GET_ONE.PENDING:
            return { ...state, loading: true }

        case ACTIONS.REMINDERS.GET_ONE.SUCCESS:
            return { ...state, reminder: action.payload.reminder, loading: false }

        case ACTIONS.REMINDERS.CREATE.SUCCESS:
            return { ...state, redirect: true }

        case ACTIONS.REMINDERS.CREATE.FAILED:
            return { ...state, reminders: { ...state.reminders, errors: action.payload.errors } }

        case ACTIONS.REMINDERS.UPDATE.SUCCESS:
            return { ...state, redirect: true }

        case ACTIONS.REMINDERS.UPDATE.FAILED:
            return { ...state, reminders: { ...state.reminders, errors: action.payload.errors } }

        case ACTIONS.REMINDERS.DELETE.SUCCESS:
            return { ...state, reminders: { ...state.reminders, list: state.reminders.list.filter(({ id }) => id !== action.payload.reminder.id) } }

        case ACTIONS.REMINDERS.CHANGE_EXECUTED:
            return { ...state, reminders: { ...state.reminders, executed: action.payload } }

        case ACTIONS.REMINDERS.CHANGE_TODAY:
            return { ...state, reminders: { ...state.reminders, today: action.payload } }

        case ACTIONS.REMINDERS.DONE.SUCCESS:
            return {
                ...state, reminders: {
                    ...state.reminders,
                    list: state.reminders.list.filter(({id}) => id !== action.payload.reminder.id),
                    errors: {}
                },
                todayReminders: {
                    list: state.reminders.list.filter(({id}) => id !== action.payload.reminder.id),
                    refresh: true
                },
                reminderModal: {show: false, id: null}
            }

        case ACTIONS.REMINDERS.DONE.FAILED:
            return { ...state, reminders:  { ...state.reminders, errors: action.payload } }

        case ACTIONS.REMINDERS.MODAL.SHOW:
            return { ...state, reminderModal: { show: true, id: action.payload } }

        case ACTIONS.REMINDERS.MODAL.HIDE:
            return { ...state, reminderModal: { show: false, id: null } }


        case ACTIONS.REDIRECT_FALSE:
            return { ...state, redirect: false }

        default:
            return state;
    }
}