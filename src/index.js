import React from 'react'
import ReactDOM from 'react-dom'
import registerServiceWorker from './registerServiceWorker'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { syncHistoryWithStore } from "react-router-redux"
import { createBrowserHistory } from "history"
import combineReducers from "./reducer"
import Root from './components/Root'
import { initialize, addTranslationForLanguage } from 'react-localize-redux'
import HTTP from "./utils/HTTP"

const store = createStore(combineReducers, applyMiddleware(thunk))
const history = syncHistoryWithStore(createBrowserHistory(), store)

async function getTranslates() {
    let response = await HTTP.get('/api/translates')
    return response.data.translates
}

async function initTranslates() {
    try {
        const translates = await getTranslates()
        const languages = ['en', 'uk']

        store.dispatch(initialize(languages, {defaultLanguage: 'en'}))

        store.dispatch(addTranslationForLanguage(translates.en, 'en'))
        store.dispatch(addTranslationForLanguage(translates.uk, 'uk'))
        // console.log(store.getState())

        ReactDOM.render(<Root history={history} store={store} />, document.getElementById('root'))
    } catch (error) {
        console.log(error)
    }
}

initTranslates()



registerServiceWorker()

