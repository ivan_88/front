import React, { Component } from 'react'
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { getTranslate } from 'react-localize-redux'
import { Col, Row, Table, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { getUsersPartnersListAction, getUsersPartnersCountAction, deleteUserPartnerAction } from "../../actions/actionsUsersPartners"
import { hasRole } from "../../utils/checkRoles"
import Loader from '../parts/Loader'

class UsersPartnersList extends Component {
    constructor(props) {
        super(props)

        this.archive = this.archive.bind(this)
    }

    componentDidMount() {
        this.props.getUsersPartnersListAction()
        this.props.getUsersPartnersCountAction()
    }

    archive(id) {
        this.props.deleteUserPartnerAction(id)
        this.props.getUsersPartnersCountAction()
    }

    render() {
        const { translate, authUser, users, partnersCreated, partnersCanCreate } = this.props
        const archive = this.archive

        return (
            <div className="content">
                <div className="container-fluid">
                    <Row>
                        <Col md={12}>
                            <div className="card card-plain">

                                <Row className="card-header"
                                     style={{
                                         backgroundImage: "linear-gradient(60deg, rgba(38, 198, 218, 0.6), rgba(0, 172, 193, 0.6))",
                                         paddingBottom: '20px'
                                     }}>
                                    <Col xs={6} >
                                        <h4 className="title">
                                            { translate('interface.network') }
                                        </h4>
                                        <div>
                                            <h5>
                                                { translate('interface.partners_created') }: { partnersCreated }
                                            </h5>
                                            <h5>
                                                { translate('interface.partners_can_create') }: { hasRole(authUser, 'admin')
                                                ? translate('interface.unlimited')
                                                : partnersCanCreate }
                                            </h5>
                                        </div>

                                        { partnersCanCreate || hasRole(authUser, 'admin') &&
                                             <Link to={`/partners/users/create`} className="btn btn-default">
                                                <i className={`fa fa-plus-circle`} />&nbsp;
                                                    { translate('interface.create') }
                                            </Link>
                                         }

                                    </Col>
                                </Row>

                                <div className="card-content table-responsive">
                                    <Loader />
                                    {users && users.length > 0 &&
                                        <Table className="table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>{ translate('interface.name') }</th>
                                                <th>{ translate('interface.email') }</th>
                                                { hasRole(authUser, 'admin') &&
                                                    <th>{ translate('interface.parent') }</th>
                                                }
                                                <th>{ translate('interface.actions') }</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            {users.map(function (user) {
                                                return (
                                                <tr key={user.id}>
                                                    <td>{user.name}</td>
                                                    <td>{user.email}</td>
                                                    { hasRole(authUser, 'admin') &&
                                                        <td>{ user.parent.name }</td>
                                                    }
                                                    <td>
                                                        <Link to={`/partners/users/show/${user.id}`}
                                                              type="button"
                                                              rel="tooltip"
                                                              title= { translate('interface.show') }
                                                              className="btn btn-default fa fa-eye"
                                                        />
                                                        { (hasRole(authUser, 'admin') || user.parent_id === authUser.id) &&
                                                            <Link to={`/partners/users/edit/${user.id}`}
                                                                  type="button"
                                                                  rel="tooltip"
                                                                  title= { translate('interface.edit') }
                                                                  className="btn btn-default fa fa-edit"
                                                            />
                                                        }
                                                        { (hasRole(authUser, 'admin') || user.parent_id === authUser.id) &&
                                                            <Button rel="tooltip"
                                                                    onClick={() => archive(user.id)}
                                                                    title= { translate('interface.delete') }
                                                                    className="btn btn-default fa fa-archive"
                                                            />
                                                            }
                                                    </td>
                                                </tr>
                                                )
                                            })}
                                            </tbody>

                                        </Table>
                                        }
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}
function mapStateToProps(state, history) {
    return {
        authUser: state.reducer.authUser,
        users: state.reducer.usersPartners.list,
        partnersCreated: state.reducer.partnersCreated,
        partnersCanCreate: state.reducer.partnersCanCreate,
        history: history,
        translate: getTranslate(state.locale)
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getUsersPartnersListAction: bindActionCreators(getUsersPartnersListAction, dispatch),
        getUsersPartnersCountAction: bindActionCreators(getUsersPartnersCountAction, dispatch),
        deleteUserPartnerAction: bindActionCreators(deleteUserPartnerAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersPartnersList)