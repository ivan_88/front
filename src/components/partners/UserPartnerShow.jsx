import React, { Component } from 'react'
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { getTranslate } from 'react-localize-redux'
import { Col, Row, Table, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { getUserPartnerOneAction } from "../../actions/actionsUsersPartners"
import Loader from '../parts/Loader'

class UserPartnerShow extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.props.getUserPartnerOneAction(this.props.match.params.id)
    }

    render() {
        const { translate, user, getUserPartnerOneAction } = this.props
        console.log(user)

        return (
            <div className="content">
                <div className="container-fluid">
                    <Row>
                        <Col md={12}>
                            <div className="card card-plain">
                                <Row className="card-header"
                                     style={{
                                         backgroundImage: "linear-gradient(60deg, rgba(38, 198, 218, 0.6), rgba(0, 172, 193, 0.6))",
                                         paddingBottom: '20px'
                                     }}>
                                    <Col xs={6} >
                                        <h4 className="title">
                                            { user.user.name }
                                            </h4>
                                            <p className="category">
                                        </p>
                                    </Col>
                                </Row>
                                <Loader />
                                <div style={{ paddingTop: '20px', paddingBottom: '20px' }}>
                                    <Row>
                                        <Col md={3}>
                                            <b>
                                                { translate('interface.name') }:
                                            </b>
                                        </Col>
                                        <Col md={3}>
                                            { user.user.name }</Col>
                                        <Col md={3}>
                                            <b>
                                                { translate('interface.email') }:
                                            </b>
                                        </Col>
                                        <Col md={3}>
                                            { user.user.email }</Col>
                                    </Row>
                                    <Row>
                                        <Col md={3}>
                                            <b>
                                                { translate('interface.created') }:
                                            </b>
                                        </Col>
                                        <Col md={3}>
                                            { user.user.created_at }</Col>
                                        <Col md={3}>
                                            <b>
                                                { translate('interface.parent') }:
                                            </b>
                                        </Col>
                                        <Col md={3}>
                                            { (Object.keys(user.user).length > 0 && user.user.parent ) &&
                                                <Link to={`/partners/users/show/${user.user.parent.id}`}
                                                      onClick={() => getUserPartnerOneAction(user.user.parent.id)}>
                                                    { user.user.parent.name }
                                                </Link>
                                            }
                                        </Col>
                                    </Row>
                                </div>

                                <h4>
                                    { translate('interface.referrals') }:
                                </h4>

                                <div className="card-content table-responsive">
                                    { (user.children && user.children.length > 0) &&
                                        <Table className="table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>{ translate('interface.name') }</th>
                                                <th>{ translate('interface.email') }</th>
                                                <th>{ translate('interface.actions') }</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            { user.children.map(function (child) {
                                                return (
                                                    <tr key={child.id}>
                                                        <td>{child.name}</td>
                                                        <td>{child.email}</td>
                                                        <td>
                                                            <Link to={`/partners/users/show/${child.id}`}
                                                                  onClick={() => getUserPartnerOneAction(child.id)}
                                                                  type="button"
                                                                  rel="tooltip"
                                                                  title= { translate('interface.show') }
                                                                  className="btn btn-default fa fa-eye"
                                                            />
                                                        </td>
                                                    </tr>
                                                )
                                            })}
                                            </tbody>
                                        </Table>
                                    }

                                        </div>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }



}
function mapStateToProps(state, history) {
    return {
        authUser: state.reducer.authUser,
        user: state.reducer.userPartner,
        partnersCreated: state.reducer.partnersCreated,
        partnersCanCreate: state.reducer.partnersCanCreate,
        history: history,
        translate: getTranslate(state.locale)
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getUserPartnerOneAction: bindActionCreators(getUserPartnerOneAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserPartnerShow)