import React, { Component } from 'react';
import { Col, Row, Table, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { getTranslate } from 'react-localize-redux'
import { getUsersListAction, deleteUserAction } from '../../actions/actionsUsers'
import Pagination from "../parts/Pagination"
import Loader from '../parts/Loader'
import { getPart } from "../../utils/checkPart"

class UsersList extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.props.getUsersListAction()
    }

    render() {
        const { users, page, total, perPage, deleteUserAction, translate } = this.props
        const id = this.props.match.params.id

        return (
            <div className="content">
                <div className="container-fluid">
                    <Row>
                        <Col md={12}>
                            <div className="card card-plain">

                                <Row className="card-header"
                                     style={{
                                         backgroundImage: "linear-gradient(60deg, rgba(38, 198, 218, 0.6), rgba(0, 172, 193, 0.6))",
                                         paddingBottom: '20px'
                                     }}>
                                    <Col xs={6} >
                                        <h4 className="title">
                                            { translate('interface.users') }
                                        </h4>
                                        <Link to={`/${getPart()}/users/create`} className="btn btn-default">
                                            <i className={`fa fa-plus-circle`} />&nbsp;
                                            { translate('interface.create') }
                                        </Link>
                                    </Col>
                                </Row>

                                <div className="card-content table-responsive">
                                    { (users && users.length > 0) &&
                                        <Table className="table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>{ translate('interface.name') }</th>
                                                <th>{ translate('interface.email') }</th>
                                                <th>{ translate('interface.role') }</th>
                                                <th>{ translate('interface.actions') }</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            {users.map(function (user) {
                                                return (
                                                    <tr key={user.id}>
                                                        <td>{user.name}</td>
                                                        <td>{user.email}</td>
                                                        <td>{user.roles.length > 0 ? user.roles.map(function (role, j) {
                                                            return (<span key={j}>{role.name}</span>)
                                                        }) : null}</td>
                                                        <td className="td-actions">
                                                            <Link to={`/${getPart()}/users/edit/${user.id}`}
                                                                  type="button"
                                                                  rel="tooltip"
                                                                  title= { translate('interface.edit') }
                                                                  className="btn btn-default fa fa-edit"
                                                            />
                                                            <Button onClick={() => deleteUserAction(user.id)}
                                                                    rel="tooltip"
                                                                    title= { translate('interface.delete') }
                                                                    className="fa fa-archive"
                                                            />
                                                            <Link to={`/${getPart()}/users/log/${user.id}`}
                                                                  type="button"
                                                                  rel="tooltip"
                                                                  title={ translate('interface.log') }
                                                                  className="btn btn-default fa fa-history"
                                                            />

                                                        </td>
                                                    </tr>
                                                )
                                            })}
                                            </tbody>

                                        </Table>
                                    }
                                    <Loader />
                                </div>

                                <Pagination page={page} count={Math.ceil(total / perPage)} id={id} action={'getUsersListAction'} />
                            </div>

                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state, history) {
    return {
        users: state.reducer.users.list,
        page: state.reducer.users.page,
        perPage: state.reducer.users.perPage,
        total: state.reducer.users.total,
        history: history,
        translate: getTranslate(state.locale)
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getUsersListAction: bindActionCreators(getUsersListAction, dispatch),
        deleteUserAction: bindActionCreators(deleteUserAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersList)