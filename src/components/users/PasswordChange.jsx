import React, { Component } from 'react';
import { Col, Row, Label, Button } from 'react-bootstrap'
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { getTranslate } from 'react-localize-redux'
import { updateUserAction } from '../../actions/actionsUsers'
import Errors from "../parts/Errors"


class SelfEdit extends Component {
    constructor(props) {
        super(props)
        this.state = {
           user: {
               password: '',
               password_confirmation: '',
           }
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    handleSubmit(event) {
        event.preventDefault()
        this.props.updateUserAction(this.props.user.id, this.state.user)
        this.props.history.goBack()
    }

    handleChange(propertyName, event) {
        const user = this.state.user
        user[propertyName] = event.target.value
        this.setState({ user: user })
    }

    componentWillReceiveProps(nextProps) {
        this.state.user = nextProps.user
    }

    render() {
        const { errors, translate } = this.props

        return (
            <div className="content">
                <div className="container-fluid">
                    <Row>
                        <Col md={12}>
                            <div className="card card-plain">
                                <Row className="card-header"
                                     style={{backgroundImage: "linear-gradient(60deg, rgba(38, 198, 218, 0.6), rgba(0, 172, 193, 0.6))"}}>
                                    <Col xs={8}>
                                        <h4 className="title">{ translate('interface.change_password') }</h4>
                                        <p className="category">
                                            <Button onClick={this.props.history.goBack}
                                                    className="btn btn-sm btn-white static-color">
                                                { translate('interface.back') }
                                            </Button>
                                        </p>
                                    </Col>
                                </Row>

                                <Errors errors={errors} />

                                <div className="card-content">
                                    <Row>
                                        <Col md={6}>
                                            <div className="form-group label-floating">
                                                <Label className="control-label">{ translate('interface.password') }</Label>
                                                <input className="form-control"
                                                       type="password"
                                                       name="password"
                                                       onChange={this.handleChange}
                                                />
                                            </div>
                                        </Col>
                                        <Col md={6}>
                                            <div className="form-group label-floating">
                                                <Label className="control-label">{ translate('interface.password_confirmation') }</Label>
                                                <input className="form-control"
                                                       type="password"
                                                       name="password_confirmation"
                                                       onChange={this.handleChange}
                                                />
                                            </div>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={6}>
                                            <Button onClick={this.handleSubmit}>{ translate('interface.save') }</Button>
                                        </Col>
                                    </Row>

                                </div>

                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.reducer.authUser,
        lang: state.reducer.init.lang,
        errors: state.reducer.users.errors,
        translate: getTranslate(state.locale)
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateUserAction: bindActionCreators(updateUserAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SelfEdit)