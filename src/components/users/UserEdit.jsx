import React, { Component } from 'react';
import { Col, Row, Label, Button } from 'react-bootstrap'
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { getTranslate } from 'react-localize-redux'
import { setRedirectFalse } from '../../actions/actionsGlobal'
import { updateUserAction, getUserAction } from '../../actions/actionsUsers'
import { updateUserPartnerAction } from '../../actions/actionsUsersPartners'
import Errors from "../parts/Errors"
import { hasRole } from "../../utils/checkRoles"
import { getPart, checkPart } from "../../utils/checkPart"


class UserEdit extends Component {
    constructor(props) {
        super(props)
        this.state = {
           user: {
               name: '',
               email: '',
               role_id: ''
           }
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    handleSubmit(event) {
        event.preventDefault()
        if (checkPart('partners')) {
            this.props.updateUserPartnerAction(this.props.match.params.id, this.state.user)
        } else {
            this.props.updateUserAction(this.props.match.params.id, this.state.user)
        }
    }

    handleChange(propertyName, event) {
        const user = this.state.user
        user[propertyName] = event.target.value
        this.setState({ user: user })
    }

    componentWillReceiveProps(nextProps) {
        this.state.user = nextProps.user
        if (nextProps.redirect) {
            this.props.setRedirectFalse()
            this.props.history.push('/' + getPart() + '/users/list')
        }
    }

    componentDidMount() {
        this.props.getUserAction(this.props.match.params.id)
    }

    render() {
        const { authUser, roles, errors, translate } = this.props
        const user = this.props.user

        return (
            <div className="content">
                <div className="container-fluid">
                    <Row>
                        <Col md={12}>
                            <div className="card card-plain">
                                <Row className="card-header"
                                     style={{backgroundImage: "linear-gradient(60deg, rgba(38, 198, 218, 0.6), rgba(0, 172, 193, 0.6))"}}>
                                    <Col xs={8}>
                                        <h4 className="title">{ translate('interface.edit') } { user.name } </h4>
                                        <p className="category">
                                            <Button onClick={this.props.history.goBack}
                                                    className="btn btn-sm btn-white static-color">
                                                { translate('interface.back') }
                                            </Button>
                                        </p>
                                    </Col>
                                </Row>

                                <Errors errors={errors} />

                                <div className="card-content">
                                    <Row>
                                        <Col md={6}>
                                            <div className="form-group label-floating">
                                                <Label className="control-label">{ translate('interface.name') }</Label>
                                                <input className="form-control"
                                                       type="text"
                                                       name="name"
                                                       value={user.name}
                                                       onChange={this.handleChange.bind(this, 'name')}
                                                />
                                            </div>
                                        </Col>
                                        <Col md={6}>
                                            <div className="form-group label-floating">
                                                <Label className="control-label">{ translate('interface.email') }</Label>
                                                <input className="form-control"
                                                       type="text"
                                                       name="email"
                                                       value={user.email}
                                                       onChange={this.handleChange.bind(this, 'email')}
                                                />
                                            </div>
                                        </Col>
                                    </Row>

                                    { hasRole(authUser, 'admin')  ?
                                        <Row>
                                            <Col md={6}>
                                                <Label className="control-label">{ translate('interface.role') }</Label>
                                                <select name="role_id" className="form-control" onChange={this.handleChange.bind(this, 'role_id')} >
                                                    <option value={0}>Choose...</option>
                                                    { roles.length > 0 && roles.map(function (role) {
                                                        return (
                                                            <option key={role.id} value={role.id}  selected={hasRole(user, role.name)} >
                                                                { role.display_name }
                                                            </option>
                                                        )
                                                    }) }
                                                </select>

                                            </Col>
                                        </Row>
                                    : null }


                                    <Row>
                                        <Col md={6}>
                                            <Button style={{ marginTop: '20px' }} onClick={this.handleSubmit}>{ translate('interface.save') }</Button>
                                        </Col>
                                    </Row>

                                </div>

                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        authUser: state.reducer.authUser,
        user: state.reducer.user,
        roles: state.reducer.init.roles,
        errors: state.reducer.users.errors,
        redirect: state.reducer.redirect,
        translate: getTranslate(state.locale)
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateUserAction: bindActionCreators(updateUserAction, dispatch),
        setRedirectFalse: bindActionCreators(setRedirectFalse, dispatch),
        getUserAction: bindActionCreators(getUserAction, dispatch),
        updateUserPartnerAction: bindActionCreators(updateUserPartnerAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserEdit)