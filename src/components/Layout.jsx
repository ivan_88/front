import React, { Component } from 'react'
import { checkPart } from "../utils/checkPart"
import Sidebar from './Sidebar'
import UpNavbar from './UpNavbar'

export default class Layout extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="wrapper">
                { (checkPart('brokers') || checkPart('sales') || checkPart('partners')) &&
                    <Sidebar store={this.props.store} history={this.props.history} />
                }

                { (checkPart('brokers') || checkPart('sales') || checkPart('partners')) &&
                    <UpNavbar store={this.props.store} history={this.props.history} />
                }

                { this.props.children }
            </div>
        )
    }
}