import React, { Component } from 'react';
import { Col, Row, Label, Button } from 'react-bootstrap'
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { getTranslate } from 'react-localize-redux'
import { setRedirectFalse } from '../../actions/actionsGlobal'
import { updateInterestAction, getInterestAction } from '../../actions/actionsInterests'
import Errors from "../parts/Errors"
import Loader from '../parts/Loader'
import {getPart} from "../../utils/checkPart";

class InterestEdit extends Component {
    constructor(props) {
        super(props)
        this.state = {
           interest: {
               name_en: '',
               name_uk: '',
               project_id: ''
           }
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    handleSubmit(event) {
        event.preventDefault()
        this.props.updateInterestAction(this.props.match.params.id, this.state.interest)
    }

    handleChange(propertyName, event) {
        const interest = this.state.interest
        interest[propertyName] = event.target.value
        this.setState({ interest: interest })
    }

    componentWillReceiveProps(nextProps) {
        this.state.interest = nextProps.interest
        if (nextProps.redirect) {
            this.props.setRedirectFalse()
            this.props.history.push('/' + getPart() + '/interests/list')
        }
    }

    componentDidMount() {
        this.props.getInterestAction(this.props.match.params.id)
    }

    render() {
        const { interest, projects, errors, translate } = this.props

        if (interest) {
            return (
                <div className="content">
                    <div className="container-fluid">
                        <Row>
                            <Col md={12}>
                                <div className="card card-plain">
                                    <Row className="card-header"
                                         style={{backgroundImage: "linear-gradient(60deg, rgba(38, 198, 218, 0.6), rgba(0, 172, 193, 0.6))"}}>
                                        <Col xs={8}>
                                            <h4 className="title">{ translate('interface.edit') } { interest.name_en } - { interest.name_uk } </h4>
                                            <p className="category">
                                                <Button onClick={this.props.history.goBack}
                                                        className="btn btn-sm btn-white static-color">
                                                    { translate('interface.back') }
                                                </Button>
                                            </p>
                                        </Col>
                                    </Row>

                                    <Errors errors={errors} />
                                    <Loader />

                                    <div className="card-content">
                                        <Row>
                                            <Col md={6}>
                                                <div className="form-group label-floating">
                                                    <Label className="control-label">{ translate('interface.name_en') }</Label>
                                                    <input className="form-control"
                                                           type="text"
                                                           name="name_en"
                                                           value={interest.name_en}
                                                           onChange={this.handleChange.bind(this, 'name_en')}
                                                    />
                                                </div>
                                            </Col>
                                            <Col md={6}>
                                                <div className="form-group label-floating">
                                                    <Label className="control-label">{ translate('interface.name_uk') }</Label>
                                                    <input className="form-control"
                                                           type="text"
                                                           name="name_uk"
                                                           value={interest.name_uk}
                                                           onChange={this.handleChange.bind(this, 'name_uk')}
                                                    />
                                                </div>
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={6}>
                                                <Label className="control-label">{ translate('interface.project') }</Label>
                                                <select name="project_id" className="form-control" onChange={this.handleChange.bind(this, 'project_id')} >
                                                    <option value={0}>Choose...</option>
                                                    { projects.length > 0 && projects.map(function (project) {
                                                        return (
                                                            <option key={project.id}
                                                                    value={project.id}
                                                                    selected={project.id === interest.project_id} >
                                                                { project.name_en } - { project.name_uk }
                                                            </option>
                                                        )
                                                    }) }
                                                </select>

                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={6}>
                                                <Button style={{ marginTop: '20px' }} onClick={this.handleSubmit}>{ translate('interface.save') }</Button>
                                            </Col>
                                        </Row>

                                    </div>

                                </div>
                            </Col>
                        </Row>
                    </div>
                </div>
            )
        } else {
            return null
        }

    }
}

function mapStateToProps(state) {
    return {
        interest: state.reducer.interest,
        projects: state.reducer.init.projects,
        errors: state.reducer.interests.errors,
        redirect: state.reducer.redirect,
        translate: getTranslate(state.locale)
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateInterestAction: bindActionCreators(updateInterestAction, dispatch),
        setRedirectFalse: bindActionCreators(setRedirectFalse, dispatch),
        getInterestAction: bindActionCreators(getInterestAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(InterestEdit)