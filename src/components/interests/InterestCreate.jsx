import React, { Component } from 'react';
import { Col, Row, Label, Button } from 'react-bootstrap'
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { getTranslate } from 'react-localize-redux'
import { setRedirectFalse } from '../../actions/actionsGlobal'
import { createInterestAction } from '../../actions/actionsInterests'
import Errors from "../parts/Errors"
import {getPart} from "../../utils/checkPart";


class InterestCreate extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name_en: '',
            name_uk: '',
            project_id: ''
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.createInterestAction(this.state)
    }

    handleChange(event) {
        this.state[event.target.name] = event.target.value
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.redirect) {
            this.props.setRedirectFalse()
            this.props.history.push('/' + getPart() + '/interests/list')
        }
    }

    render() {
        const { projects, errors, translate } = this.props
        return (
            <div className="content">
                <div className="container-fluid">
                    <Row>
                        <Col md={12}>
                            <div className="card card-plain">
                                <Row className="card-header"
                                     style={{backgroundImage: "linear-gradient(60deg, rgba(38, 198, 218, 0.6), rgba(0, 172, 193, 0.6))"}}>
                                    <Col xs={8}>
                                        <h4 className="title">{ translate('interface.create_interest') }</h4>
                                        <p className="category">
                                            <Button onClick={this.props.history.goBack}
                                                    className="btn btn-sm btn-white static-color">
                                                { translate('interface.back') }
                                            </Button>
                                        </p>
                                    </Col>
                                </Row>

                                <Errors errors={errors} />

                                <div className="card-content">
                                    <Row>
                                        <Col md={6}>
                                            <div className="form-group label-floating">
                                                <Label className="control-label">{ translate('interface.name_en') }</Label>
                                                <input className="form-control"
                                                       type="text"
                                                       name="name_en"
                                                       onChange={this.handleChange}
                                                />
                                            </div>
                                        </Col>
                                        <Col md={6}>
                                            <div className="form-group label-floating">
                                                <Label className="control-label">{ translate('interface.name_uk') }</Label>
                                                <input className="form-control"
                                                       type="text"
                                                       name="name_uk"
                                                       onChange={this.handleChange}
                                                />
                                            </div>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={6}>
                                            <Label className="control-label">{ translate('interface.project') }</Label>
                                            <select name="project_id" className="form-control" onChange={this.handleChange}>
                                                <option value={0}>Choose...</option>
                                                { projects.length > 0 && projects.map(function (project) {
                                                    return (
                                                        <option key={project.id} value={project.id} >
                                                        { project.name_en } - { project.name_uk }
                                                        </option>
                                                    )
                                                }) }
                                            </select>

                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={6}>
                                            <Button style={{ marginTop: '20px' }} onClick={this.handleSubmit}>{ translate('interface.create') }</Button>
                                        </Col>
                                    </Row>

                                </div>

                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        projects: state.reducer.init.projects,
        errors: state.reducer.interests.errors,
        redirect: state.reducer.redirect,
        translate: getTranslate(state.locale)
    }
}

function mapDispatchToProps(dispatch) {
    return {
        createInterestAction: bindActionCreators(createInterestAction, dispatch),
        setRedirectFalse: bindActionCreators(setRedirectFalse, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(InterestCreate)