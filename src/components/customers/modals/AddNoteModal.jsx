import React, { Component } from "react"
import { Modal, Label, Row, Col, Button } from 'react-bootstrap'
import { getTranslate } from "react-localize-redux"
import { connect } from "react-redux"
import { addNoteAction, hideAddNoteModal } from "../../../actions/actionsCustomers";
import { bindActionCreators } from "redux"
import Errors from "../../parts/Errors"

class AddNoteModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            note: ''
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.addNoteAction(this.props.id, { note: this.state.note })
        this.state.note = ''
    }

    handleChange(event) {
        this.state[event.target.name] = event.target.value
    }

    render() {
        const { show, errors, translate, hideAddNoteModal } = this.props

        return (
            <Modal backdrop={true} show={show} aria-labelledby="ModalHeader" onBackdropClick={() => hideAddNoteModal()}>
                <Modal.Header>
                    <h4>
                        { translate('interface.add_note') }
                    </h4>
                </Modal.Header>
                <Modal.Body>

                    <Errors errors={errors} />

                        <Row>
                            <Col md={12}>
                                <Label className="control-label">{ translate('interface.note') }</Label>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12}>
                                <textarea id="reason"
                                          name="note"
                                          className="form-control"
                                          onChange={this.handleChange}>

                                </textarea>
                            </Col>
                        </Row>

                </Modal.Body>
                <Modal.Footer>
                    <Button className="btn-info" onClick={() => hideAddNoteModal()}>
                        { translate('interface.cancel') }
                    </Button>
                    <Button className="btn-danger" onClick={this.handleSubmit} >
                        { translate('interface.add_note') }
                    </Button>
                </Modal.Footer>
            </Modal>
        )
    }
}
function mapStateToProps(state, history) {
    return {
        show: state.reducer.customers.modalAddNote.show,
        id: state.reducer.customers.modalAddNote.id,
        errors: state.reducer.customers.errors,
        translate: getTranslate(state.locale)
    }
}

function mapDispatchToProps(dispatch) {
    return {
        addNoteAction: bindActionCreators(addNoteAction, dispatch),
        hideAddNoteModal: bindActionCreators(hideAddNoteModal, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddNoteModal)