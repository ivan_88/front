import React, { Component } from "react"
import { Modal, Table, Row, Col, Button } from 'react-bootstrap'
import { getTranslate } from "react-localize-redux"
import { connect } from "react-redux"
import { hideNotesListModalAction } from "../../../actions/actionsCustomers"
import { bindActionCreators } from "redux"
import Pagination from '../../parts/Pagination'
import Loader from '../../parts/Loader'
import moment from "moment/moment"

class NotesListModal extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { id, notes, page, perPage, total, show, translate, hideNotesListModalAction } = this.props

        return (
            <Modal backdrop={true} show={show} aria-labelledby="ModalHeader" onBackdropClick={() => hideNotesListModalAction()}>
                <Modal.Header>
                    <h4>
                        { translate('interface.notes') }
                    </h4>
                </Modal.Header>
                <Modal.Body>

                    { notes.length > 0 &&
                        <div>
                            <Row>
                                <Col md={12}>
                                    <Table md={12}>

                                        <thead>
                                        <tr>
                                            <th>
                                                <b>{translate('interface.user')}</b>
                                            </th>
                                            <th>
                                                <b>{translate('interface.text')}</b>
                                            </th>
                                            <th>
                                                <b>{translate('interface.date')}</b>
                                            </th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        { notes.map(function (note) {
                                            return (
                                                <tr key={note.id}>
                                                    <td>
                                                        {note.user.name}
                                                    </td>
                                                    <td>
                                                        {note.text}
                                                    </td>
                                                    <td>
                                                        {moment(note.created_at).format('DD-MM-YYYY HH:mm')}
                                                    </td>
                                                </tr>
                                            )
                                        })}

                                        </tbody>
                                    </Table>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12}>
                                    <Pagination id={id} page={page} count={Math.ceil(total / perPage)}
                                                action={'getNotesListAction'}/>
                                </Col>
                            </Row>
                        </div>
                    }
                            <Loader />
                </Modal.Body>
                <Modal.Footer>
                    <Button className="btn-info" onClick={() => hideNotesListModalAction()}>
                        { translate('interface.close') }
                    </Button>
                </Modal.Footer>
            </Modal>
        )
    }
}
function mapStateToProps(state) {
    return {
        show: state.reducer.customers.modalNotesList.show,
        notes: state.reducer.notes.data,
        id: state.reducer.customers.modalNotesList.id,
        page: state.reducer.notes.page,
        perPage: state.reducer.notes.perPage,
        total: state.reducer.notes.total,
        translate: getTranslate(state.locale)
    }
}

function mapDispatchToProps(dispatch) {
    return {
        hideNotesListModalAction: bindActionCreators(hideNotesListModalAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NotesListModal)