import React, { Component } from "react"
import { Modal, Label, Row, Col, Button } from 'react-bootstrap'
import { getTranslate } from "react-localize-redux"
import { connect } from "react-redux"
import { activeCustomerAction, hideCustomersActiveModal } from "../../../actions/actionsCustomers";
import { bindActionCreators } from "redux"
import Errors from "../../parts/Errors"

class ActiveOrArchiveModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            reason: ''
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.activeCustomerAction(this.props.id, { reason: this.state.reason, action: this.props.action })
    }

    handleChange(event) {
        this.state[event.target.name] = event.target.value
    }

    render() {
        const { show, action, errors, translate, hideCustomersActiveModal } = this.props

        return (
            <Modal backdrop={true} show={show} aria-labelledby="ModalHeader" onBackdropClick={() => hideCustomersActiveModal}>
                <Modal.Header>
                    <h4>
                        {action === 0 ? translate('interface.to_archive') : translate('interface.to_active')}
                    </h4>
                </Modal.Header>
                <Modal.Body>

                    <Errors errors={errors} />

                        <Row>
                            <Col md={12}>
                                <Label className="control-label">{ translate('interface.reason') }</Label>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12}>
                                <textarea id="reason"
                                          name="reason"
                                          className="form-control"
                                          onChange={this.handleChange}>

                                </textarea>
                            </Col>
                        </Row>

                </Modal.Body>
                <Modal.Footer>
                    <Button className="btn-info" onClick={() => hideCustomersActiveModal()}>
                        { translate('interface.cancel') }
                    </Button>
                    <Button className="btn-danger" onClick={this.handleSubmit} >
                        {action === 0 ? translate('interface.to_archive') : translate('interface.to_active')}
                    </Button>
                </Modal.Footer>
            </Modal>
        )
    }
}
function mapStateToProps(state, history) {
    return {
        show: state.reducer.customers.modalActiveOrArchive.show,
        action: state.reducer.customers.modalActiveOrArchive.action,
        id: state.reducer.customers.modalActiveOrArchive.id,
        errors: state.reducer.customers.errors,
        translate: getTranslate(state.locale)
    }
}

function mapDispatchToProps(dispatch) {
    return {
        activeCustomerAction: bindActionCreators(activeCustomerAction, dispatch),
        hideCustomersActiveModal: bindActionCreators(hideCustomersActiveModal, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ActiveOrArchiveModal)