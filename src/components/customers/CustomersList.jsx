import React, { Component } from 'react'
import {Col, Row, Table, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import _ from 'lodash'
import { getTranslate, getActiveLanguage  } from 'react-localize-redux'
import { getCustomersListAction, moveCustomerAction, changeTypeAction, changeActiveAction, activeCustomerAction,
    sortCustomersAction, showAddNoteModal, showNotesListModalAction, getNotesListAction } from '../../actions/actionsCustomers'
import ActiveOrArchive from './parts/ActiveOrArchive'
import ActiveOrArchiveModal from './modals/ActiveOrArchiveModal'
import AddNoteModal from './modals/AddNoteModal'
import SearchIconsTypes from './parts/SearchIconsTypes'
import SearchIconsActive from './parts/SearchIconsActive'
import Panel from './parts/Panel'
import Filters from './parts/Filters'
import Pagination from "../parts/Pagination"
import NotesListModal from './modals/NotesListModal'
import Loader from '../parts/Loader'
import { getPart } from "../../utils/checkPart"
import { hasRole, canMoveForvard } from "../../utils/checkRoles"


class CustomersList extends Component {
    constructor(props) {
        super(props)
        this.sort = this.sort.bind(this)
        this.getNotesList = this.getNotesList.bind(this)
    }

    componentDidMount() {
        const { type, currentProject, active, order_by, order_direction } = this.props
        this.props.changeTypeAction(this.props.type)
        this.props.getCustomersListAction({ type, project: currentProject.id, active, order_by, order_direction, app: getPart() })
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.active != nextProps.active) {
            this.props.getCustomersListAction({ type: this.props.type, project: this.props.currentProject.id, active: nextProps.active, app: getPart() })
        }
        if (this.props.currentProject.id != nextProps.currentProject.id) {
            this.props.getCustomersListAction({ type: this.props.type, project: nextProps.currentProject.id, active: this.props.active, app: getPart() })
        }
    }

    sort(order_by, order_direction) {
        const { type, currentProject, active } = this.props
        this.props.sortCustomersAction(order_by, order_direction)
        this.props.getCustomersListAction({ type, project: currentProject.id, active, order_by, order_direction, app: getPart() })
    }

    getNotesList(id) {
        this.props.showNotesListModalAction(id)
        this.props.getNotesListAction({ id })
    }

    actionsByType = {
        1: {forward: 1, back: null},
        2: {forward: 2, back: 6},
        3: {forward: 3, back: 5},
        4: {forward: null, back: 4},
    }

    render() {
        const { types, type, active, customers, page, perPage, total, currentProject, contacts, authUser,
            translate, currentLanguage, order_by, order_direction, changeActiveAction, showAddNoteModal } = this.props

        const getNotesList = this.getNotesList

        const moveCustomerAction = this.props.moveCustomerAction

        const nameField = currentLanguage !== undefined ? 'name_' + currentLanguage : 'name_en'
        const actionsByType = this.actionsByType

        if (customers && contacts.length > 0) {
            return (
                <div className="content">
                    <div className="container-fluid">

                        <ActiveOrArchiveModal translate={translate} />
                        <AddNoteModal translate={translate} />
                        <NotesListModal translate={translate} />

                        <Row>
                            <Col md={12}>
                                <div className="card card-plain">
                                    <Panel changeActiveAction={changeActiveAction} types={types} type={type}
                                           active={active} translate={translate} />
                                </div>

                                <div className="card-content table-responsive">
                                    <Filters translate={translate} currentLanguage={currentLanguage} />
                                    <Table className="table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th style={{ maxWidth: '50px' }}>
                                                <span rel="tooltip"
                                                      className="pseudolink"
                                                      title={translate('interface.sort_by_id')}
                                                      onClick={() => this.sort('id', order_direction === 'asc' ? 'desc' : 'asc')}>
                                                    ID
                                                    {order_by === 'id' &&
                                                    <i className={`fa fa-sort-numeric-${order_direction}`}
                                                       aria-hidden="true"/>}
                                                </span>
                                            </th>
                                            <th>
                                                <span rel="tooltip"
                                                      className="pseudolink"
                                                      title={translate('interface.sort_by_name')}
                                                      onClick={() => this.sort('lastname', order_direction === 'asc' ? 'desc' : 'asc')}
                                                >
                                                    { translate('interface.name') }
                                                    {order_by === 'lastname' && <i className={`fa fa-sort-numeric-${order_direction}`}
                                                                                  aria-hidden="true"/> }
                                                </span>
                                            </th>
                                            <th>
                                                <span className="pseudolink">
                                                    { translate('interface.contacts') }
                                                </span>
                                            </th>
                                            <th>
                                                <span rel="tooltip"
                                                      className="pseudolink"
                                                      title={ translate('interface.sort_by_sales_person') }
                                                      onClick={() => this.sort('user_id', order_direction === 'asc' ? 'desc' : 'asc') }>
                                                    { translate('interface.sales_person') }
                                                    {order_by === 'user_id' && <i className={`fa fa-sort-numeric-${order_direction}`}
                                                                                 aria-hidden="true"/> }
                                                </span>
                                            </th>
                                            <th>
                                                <span rel="tooltip"
                                                      className="pseudolink"
                                                      title={ translate('interface.sort_by_interest') }
                                                      onClick={() => this.sort('interest_id', order_direction === 'asc' ? 'desc' : 'asc')}>
                                                    { translate('interface.interest') }
                                                    {order_by === 'interest_id' && <i className={`fa fa-sort-numeric-${order_direction}`}
                                                                                     aria-hidden="true"/> }
                                                </span>
                                            </th>
                                            <th>
                                                <span rel="tooltip"
                                                      className="pseudolink"
                                                      title={ translate('interface.sort_by_source') }
                                                      onClick={() => this.sort('source_id', order_direction === 'asc' ? 'desc' : 'asc')}>
                                                    { translate('interface.source') }
                                                    {order_by === 'source_id' &&
                                                        <i className={`fa fa-sort-numeric-${order_direction}`}
                                                           aria-hidden="true"/> }
                                                </span>
                                            </th>
                                            <th>
                                                <span rel="tooltip"
                                                      className="pseudolink"
                                                      title={ translate('interface.sort_by_contact_by') }
                                                      onClick={() => this.sort('contact_by', order_direction === 'asc' ? 'desc' : 'asc')}>
                                                    { translate('interface.contact_by') }
                                                    {order_by === 'contact_by' &&
                                                        <i className={`fa fa-sort-numeric-${order_direction}`}
                                                           aria-hidden="true"/> }
                                                </span>
                                            </th>
                                            {active === 0 &&
                                            <th>
                                                <span className="pseudolink">{translate('interface.reason')}</span>
                                            </th>
                                            }

                                            <th className="th-actions">
                                                 <span className="pseudolink">
                                                    { translate('interface.actions') }
                                                 </span>
                                            </th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        {
                                            customers.length > 0 && customers.map(function (customer) {
                                                return (
                                                    <tr key={customer.id}>
                                                        <td> {customer.id}</td>
                                                        <td>
                                                            {type === 0 ?
                                                                <SearchIconsActive active={customer.active} translate={translate} /> : null}

                                                            {type === 0 ?
                                                                <SearchIconsTypes type={customer.type} /> : null}

                                                            {customer.firstname} {customer.lastname}
                                                        </td>
                                                        <td>
                                                            {customer.phone}
                                                        </td>
                                                        <td>
                                                            {customer.user.name ? customer.user.name : 'DELETED'}
                                                        </td>
                                                        <td>{customer.interest[nameField]}</td>
                                                        <td>{customer.source[nameField]}</td>
                                                        <td> {_.find(contacts, ['id', customer.contact_by])[currentLanguage]}  </td>

                                                        {active === 0 &&
                                                            <td>{customer.rejection_reason}</td>
                                                        }

                                                        <td className="td-actions">
                                                            <Link to={`/${getPart()}/customers/edit/${customer.id}`}
                                                                  type="button"
                                                                  rel="tooltip"
                                                                  title={ translate('interface.edit') }
                                                                  className="btn btn-default fa fa-edit"
                                                            />

                                                            {
                                                                customer.type > 1 &&
                                                                    <Button
                                                                        type="button"
                                                                        rel="tooltip"
                                                                        title={`${ translate('interface.move_to') } ${types[customer.type - 1]}`}
                                                                        onClick={() => moveCustomerAction({
                                                                            id: customer.id,
                                                                            action: actionsByType[customer.type].back
                                                                        })}
                                                                        className={`btn btn-simple fa fa-arrow-left color_${types[customer.type - 1]}`}
                                                                    />
                                                            }
                                                            { canMoveForvard(authUser, customer.type) &&
                                                                <Button
                                                                    type="button"
                                                                    rel="tooltip"
                                                                    title={`${ translate('interface.move_to') } ${types[customer.type + 1]}`}
                                                                    onClick={() => moveCustomerAction({
                                                                        id: customer.id,
                                                                        action: actionsByType[customer.type].forward
                                                                    })}
                                                                    className={`fa fa-arrow-right color_${types[customer.type + 1]}`}
                                                                />
                                                            }

                                                            <ActiveOrArchive customer={customer} authUser={authUser} translate={translate} />

                                                            { hasRole(authUser, 'admin') &&
                                                                <Link to={`/${getPart()}/customers/log/${customer.id}`}
                                                                      type="button"
                                                                      rel="tooltip"
                                                                      title={ translate('interface.log') }
                                                                      className="btn btn-default fa fa-history"
                                                                />
                                                            }

                                                            <Button type="button"
                                                                    rel="tooltip"
                                                                    title={ translate('interface.add_note') }
                                                                    onClick={() => showAddNoteModal({ id: customer.id })}
                                                                    className="fa fa-sticky-note"
                                                            />

                                                            <Button type="button"
                                                                    rel="tooltip"
                                                                    title={ translate('interface.notes') }
                                                                    onClick={() => getNotesList(customer.id)}
                                                                    className="fa fa-list"
                                                            />

                                                        </td>
                                                    </tr>
                                                )
                                                })}

                                        </tbody>
                                    </Table>
                                </div>
                                <Loader />
                                <Pagination page={page} count={Math.ceil(total / perPage)} active={active} type={type}
                                            projectId={currentProject.id} action={'getCustomersListAction'} order_by={order_by}
                                            order_direction={order_direction} />
                            </Col>
                        </Row>
                    </div>
                </div>
            )
        } else {
            return null
        }
    }
}

function mapStateToProps(state, history) {
    return {
        customers: state.reducer.customers.list,
        active: state.reducer.customers.active,
        page: state.reducer.customers.page,
        perPage: state.reducer.customers.perPage,
        total: state.reducer.customers.total,
        type: state.reducer.customers.type,
        order_by: state.reducer.customers.order_by,
        order_direction: state.reducer.customers.order_direction,
        types: state.reducer.init.types,
        currentProject: state.reducer.currentProject,
        contacts: state.reducer.init.contacts,
        authUser: state.reducer.authUser,
        modalActiveOrArchive: state.reducer.customers.modalActiveOrArchive,
        history: history,
        translate: getTranslate(state.locale),
        currentLanguage: getActiveLanguage(state.locale).code
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getCustomersListAction: bindActionCreators(getCustomersListAction, dispatch),
        moveCustomerAction: bindActionCreators(moveCustomerAction, dispatch),
        changeTypeAction: bindActionCreators(changeTypeAction, dispatch),
        changeActiveAction: bindActionCreators(changeActiveAction, dispatch),
        activeCustomerAction: bindActionCreators(activeCustomerAction, dispatch),
        sortCustomersAction: bindActionCreators(sortCustomersAction, dispatch),
        showAddNoteModal: bindActionCreators(showAddNoteModal, dispatch),
        showNotesListModalAction: bindActionCreators(showNotesListModalAction, dispatch),
        getNotesListAction: bindActionCreators(getNotesListAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomersList)