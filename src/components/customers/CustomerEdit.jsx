import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { Col, Row, Button } from 'react-bootstrap'
import { bindActionCreators } from "redux"
import { getTranslate, getActiveLanguage  } from 'react-localize-redux'
import { getCustomerAction, updateCustomerAction } from "../../actions/actionsCustomers"
import { setRedirectFalse } from "../../actions/actionsGlobal"
import { connect } from "react-redux"
import Errors from "../parts/Errors"
import Loader from '../parts/Loader'
import { getPart } from "../../utils/checkPart"

class CustomerEdit extends Component {

    constructor(props) {
        super(props)
        this.state = {
            customer: {
                firstname: '',
                lastname: '',
                phone: '',
                email: '',
                country_id: '',
                interest_id: '',
                source_id: '',
                contact_by: '',
                city: '',
                address: '',
                zip: '',
                note: '',
                project_id: '',
                app: getPart()
            }
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    handleSubmit(event) {
        event.preventDefault()
        this.props.updateCustomerAction(this.props.match.params.id, this.state.customer)
    }

    handleChange(propertyName, event) {
        const customer = this.state.customer
        customer[propertyName] = event.target.value
        this.setState({ customer: customer })
    }

    componentDidMount() {
        this.props.getCustomerAction(this.props.match.params.id)
    }

    componentWillReceiveProps(nextProps) {
        this.state.customer = nextProps.customer
        if (nextProps.redirect) {
            this.props.setRedirectFalse()
            this.props.history.push('/' + getPart() + '/customers/list')
        }
    }

    render() {
        const { interests, sources, contacts, countries, currentLanguage, errors, translate } = this.props
        const nameField = currentLanguage !== undefined ? 'name_' + currentLanguage : 'name_en'
        const customer = this.state.customer

        if (customer) {
            return (
                <div className="content">
                    <div className="container-fluid">
                        <Row>
                            <Col md={12}>
                                <div className="card card-plain">
                                    <Row className="card-header" style={{ backgroundImage: "linear-gradient(60deg, rgba(38, 198, 218, 0.6), rgba(0, 172, 193, 0.6))" }} >
                                        <Col xs={8}>
                                            <h4 className="title">{ translate('interface.edit') }: { customer.firstname } { customer.lastname }</h4>
                                            <p className="category">
                                                <Button onClick={this.props.history.goBack} className="btn btn-sm btn-white static-color">
                                                    { translate('interface.back') }
                                                </Button>
                                            </p>
                                        </Col>
                                    </Row>

                                    <Errors errors={errors} />
                                    <Loader />

                                    <div className="card-content">

                                        <form className="card-content">

                                            <Row>
                                                <Col md={3}>
                                                    <div className="form-group label-floating">
                                                        <label className="control-label">{ translate('interface.firstname') }</label>
                                                        <input className="form-control"
                                                               type="text"
                                                               name="firstname"
                                                               value={customer.firstname}
                                                               onChange={this.handleChange.bind(this, 'firstname')}
                                                        />
                                                    </div>
                                                </Col>
                                                <Col md={3}>
                                                    <div className="form-group label-floating">
                                                        <label className="control-label">{ translate('interface.lastname') }</label>
                                                        <input className="form-control"
                                                               type="text"
                                                               name="lastname"
                                                               value={customer.lastname}
                                                               onChange={this.handleChange.bind(this, 'lastname')}
                                                        />
                                                    </div>
                                                </Col>
                                                <Col md={3}>
                                                    <div className="form-group label-floating">
                                                        <label className="control-label">{ translate('interface.phone') }</label>
                                                        <input className="form-control"
                                                               type="text"
                                                               name="phone"
                                                               value={customer.phone}
                                                               onChange={this.handleChange.bind(this, 'phone')}
                                                        />
                                                    </div>
                                                </Col>
                                                <Col md={3}>
                                                    <div className="form-group label-floating">
                                                        <label className="control-label">{ translate('interface.email') }</label>
                                                        <input className="form-control"
                                                               type="text"
                                                               name="email"
                                                               value={customer.email}
                                                               onChange={this.handleChange.bind(this, 'email')}
                                                        />
                                                    </div>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={3}>
                                                    <div className="form-group">
                                                        <label className="control-label">{ translate('interface.country') }</label>
                                                        <select name="country_id"
                                                                onChange={this.handleChange.bind(this, 'country_id')}
                                                                className="form-control autoselect"
                                                        >
                                                            { countries.length && countries.map(function (country) {
                                                                return (
                                                                    <option key={country.id}
                                                                            value={country.id}
                                                                            selected={country.id == customer.country_id}
                                                                    >
                                                                        {country.full_name}
                                                                        </option>
                                                                )
                                                            }) }

                                                        </select>
                                                    </div>
                                                </Col>
                                                <Col md={3}>
                                                    <div className="form-group">
                                                        <label className="control-label">{ translate('interface.interest') }</label>
                                                        <select name="interest_id"
                                                                onChange={this.handleChange.bind(this, 'interest_id')}
                                                                className="form-control">
                                                            <option value={0}></option>
                                                            { interests.length && interests.map(function (interest) {
                                                                return (<option key={interest.id}
                                                                                selected={interest.id == customer.interest_id}
                                                                                value={interest.id}>
                                                                    {interest[nameField]}
                                                                    </option>)
                                                            }) }
                                                        </select>
                                                    </div>
                                                </Col>
                                                <Col md={3}>
                                                    <div className="form-group">
                                                        <label className="control-label">{ translate('interface.source') }</label>
                                                        <select name="source_id"
                                                                className="form-control"
                                                                onChange={this.handleChange.bind(this, 'source_id')} >
                                                            <option value={0}></option>
                                                            { sources.length && sources.map(function (source) {
                                                                return (<option key={source.id}
                                                                                selected={source.id == customer.source_id}
                                                                                value={source.id}>
                                                                    {source[nameField]}
                                                                    </option>)
                                                            }) }
                                                        </select>
                                                    </div>
                                                </Col>
                                                <Col md={3}>
                                                    <div className="form-group">
                                                        <label className="control-label">{ translate('interface.contact_by') }</label>
                                                        <select name="contact_by"
                                                                className="form-control"
                                                                onChange={this.handleChange.bind(this, 'contact_by')} >
                                                            <option value={0}></option>
                                                            { contacts.length && contacts.map(function (contact, key) {
                                                                return (<option key={key}
                                                                                selected={contact.id == customer.contact_by}
                                                                                value={contact.id}>
                                                                    {contact[currentLanguage]}
                                                                    </option>)
                                                            }) }
                                                        </select>
                                                    </div>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={3}>
                                                    <div className="form-group label-floating">
                                                        <label className="control-label">{ translate('interface.city') }</label>
                                                        <input className="form-control"
                                                               type="text"
                                                               name="city"
                                                                  value={customer.city}
                                                               onChange={this.handleChange.bind(this, 'city')}
                                                        />
                                                    </div>
                                                </Col>
                                                <Col md={3}>
                                                    <div className="form-group label-floating">
                                                        <label className="control-label">{ translate('interface.address') }</label>
                                                        <input className="form-control"
                                                               type="text"
                                                               name="address"
                                                                  value={customer.address}
                                                               onChange={this.handleChange.bind(this, 'address')}
                                                        />
                                                    </div>
                                                </Col>
                                                <Col md={3}>
                                                    <div className="form-group label-floating">
                                                        <label className="control-label">{ translate('interface.zip') }</label>
                                                        <input className="form-control"
                                                               type="text"
                                                               name="zip"
                                                                  value={customer.zip}
                                                               onChange={this.handleChange.bind(this, 'zip')}
                                                        />
                                                    </div>
                                                </Col>
                                                <Row>
                                                    <Col md={12}>
                                                        <div className="form-group">
                                                            <div className="form-group label-floating">
                                                                <label className="control-label">{ translate('interface.note') }</label>
                                                                <textarea className="form-control" rows="3" name="note"
                                                                          onChange={this.handleChange.bind(this, 'notes')} >

                                                                        </textarea>
                                                            </div>
                                                        </div>
                                                    </Col>
                                                </Row>
                                                <Button onClick={this.handleSubmit}>{ translate('interface.save') }</Button>

                                                <div className="clearfix" />
                                            </Row>
                                        </form>
                                    </div>
                                </div>
                            </Col>

                        </Row>
                    </div>
                </div>
            )
        } else {
            return null
        }

    }
}
function mapStateToProps(state) {
    return {
        customer: state.reducer.customer,
        interests: state.reducer.init.interests,
        sources: state.reducer.init.sources,
        contacts: state.reducer.init.contacts,
        countries: state.reducer.init.countries,
        currentProject: state.reducer.currentProject,
        errors: state.reducer.customers.errors,
        redirect: state.reducer.redirect,
        translate: getTranslate(state.locale),
        currentLanguage: getActiveLanguage(state.locale).code
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getCustomerAction: bindActionCreators(getCustomerAction, dispatch),
        updateCustomerAction: bindActionCreators(updateCustomerAction, dispatch),
        setRedirectFalse: bindActionCreators(setRedirectFalse, dispatch)
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CustomerEdit))