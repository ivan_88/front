import React, { Component } from "react";

export default class SearchIconsActive extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { active, translate } = this.props
        if (active === 1) {
            return (
                <a rel="tooltip" title={ translate('interface.active') }
                   className="btn btn-simple btn-xs btn_square-icon">
                    <i className="material-icons material-icon_static">flash_on</i>
                </a>
            )
        }
        return (
            <a rel="tooltip" title={ translate('interface.archive') }
               className="btn btn-simple btn-xs btn_square-icon">
                <i className="material-icons material-icon_static">save</i>
            </a>
        )
    }
}