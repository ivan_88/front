import React, { Component } from "react";

export default class SearchIconsTypes extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const type = this.props.type

        switch (type) {
            case 1:
                return (
                    <a rel="tooltip" title="LEAD"
                       className="btn btn-simple btn-xs btn_square-icon">
                        <i className="material-icons material-icon_success">assignment_returned</i>
                    </a>
                )
            case 2:
                return (
                    <a rel="tooltip" title="COM"
                       className="btn btn-simple btn-xs btn_square-icon">
                        <i className="material-icons material-icon_warning">assignment_returned</i>
                    </a>
                )
            case 3:
                return (
                    <a rel="tooltip" title="NEG"
                       className="btn btn-simple btn-xs btn_square-icon">
                        <i className="material-icons material-icon_info">assignment_returned</i>
                    </a>
                )
            case 4:
                return (
                    <a rel="tooltip" title="PRO"
                       className="btn btn-simple btn-xs btn_square-icon">
                        <i className="material-icons material-icon_danger">assignment_returned</i>
                    </a>
                )

            default:
                return (
                    <a rel="tooltip" title="?"
                       className="btn btn-simple btn-xs btn_square-icon">
                        ?
                    </a>
                )
        }
    }
}