import React, { Component } from "react"
import { Link } from 'react-router-dom'
import { Button, ButtonGroup, Col, Row } from 'react-bootstrap'
import { getPart } from "../../../utils/checkPart"

export default class Panel extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        let { types, type, active, changeActiveAction, translate } = this.props

        const colors = {
            3: {backgroundImage: "linear-gradient(60deg, rgba(38, 198, 218, 0.6), rgba(0, 172, 193, 0.6))"},
            1: {backgroundImage: "linear-gradient(60deg, rgba(102, 187, 106, 0.6), rgba(67, 160, 71, 0.6))"},
            2: {backgroundImage: "linear-gradient(60deg, rgba(255, 167, 38, 0.6), rgba(251, 140, 0, 0.6))"},
            4: {backgroundImage: "linear-gradient(60deg, rgba(239, 83, 80, 0.6), rgba(229, 57, 53, 0.6))"},
            0: {backgroundImage: "linear-gradient(60deg, rgba(155, 155, 155, 0.6), rgba(145, 145, 145, 0.6))"}
        }

        type = type && typeof type === 'number' ? type : 0

        const changeActiveTo = active === 0 ? 1 : 0

        return (
            <Row className="card-header" style={{ backgroundImage: colors[type].backgroundImage }}>
                <Col md={6}>
                    <h3 className="title"><b>{types[type]}</b>{types[type] !== 0 ? 'S' : ''}:&nbsp;
                        {active === 1 ? translate('interface.active') : translate('interface.archive')}
                    </h3>
                    <p className="category">
                        <ButtonGroup>
                        <Button
                            className="btn"
                            rel="tooltip"
                            onClick={() => changeActiveAction({ active: changeActiveTo })}
                            title={ translate('interface.to_archive') }>
                            <i className={`fa fa-${ active === 0 ? 'flash' : 'archive' }`} />&nbsp;
                            { active === 0 ? translate('interface.active') : translate('interface.archive') }
                        </Button>
                        <Link to={`/${getPart()}/customers/create`} className="btn btn-default">
                            <i className={`fa fa-plus-circle`} />&nbsp;
                            { translate('interface.create') }
                        </Link>
                        </ButtonGroup>
                    </p>
                </Col>
            </Row>
        )
    }
}