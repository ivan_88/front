import React, { Component } from "react"
import { Button, Col, Row } from 'react-bootstrap'
import {changeTypeAction, getCustomersListAction} from "../../../actions/actionsCustomers";
import { bindActionCreators } from "redux";
import  { connect } from "react-redux";

class Filters extends Component {
    constructor(props) {
        super(props)
        this.state = {
            search: '',
            field: '',
            interest_id: '',
            source_id: ''
        }

        this.handleSubmitSearch = this.handleSubmitSearch.bind(this)
        this.handleSubmitFilter = this.handleSubmitFilter.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleClear = this.handleClear.bind(this)
    }

    handleSubmitSearch(event) {
        event.preventDefault()
        const { search, field, interest_id, source_id } = this.state
        this.props.changeTypeAction({ type: 0 })
        this.props.getCustomersListAction({ search, field, interest_id, source_id })
    }

    handleSubmitFilter(event) {
        event.preventDefault()
        const { interest_id, source_id } = this.state
        const { active, type, currentProject, order_by, order_direction } = this.props
        this.props.getCustomersListAction({ interest_id, source_id, active, type, project: currentProject.id, order_by, order_direction })
    }

    handleChange(event) {
        this.state[event.target.name] = event.target.value
    }

    handleClear() {
        this.setState({ search: '', field: '', interest_id: '', source_id: '' })
        document.querySelector('#search').value = ''
        document.querySelector('#field').value = ''
        this.props.getCustomersListAction({ active: 1, type: 1, project: this.props.currentProject.id })
    }

    render() {
        const { interests, sources, currentLanguage, searchFields, translate  } = this.props
        const nameField = currentLanguage !== undefined ? 'name_' + currentLanguage : 'name_en'

        return (
            <div className="card margin-xs">
                <Col xs={12}>
                    <Col lg={6}>
                        <Row className="form_no-padding">
                            <Col sm={2}>
                                <Button className="btn-xs btn-white btn-round "
                                        rel="tooltip"
                                        data-placement="bottom"
                                        title={ translate('interface.search') }
                                        onClick={this.handleSubmitSearch}>
                                    <i style={{ fontSize: '20px', padding: '5px' }} className="fa fa-search" />
                                </Button>
                            </Col>
                            <Col sm={5}>
                                <select name="field" id="field" className="form-control" onChange={this.handleChange}>
                                    <option value={0}>{ translate('interface.search') }: </option>
                                    {searchFields.length && searchFields.map(function (field, key) {
                                        return (<option key={key} value={field.field}>{field[currentLanguage]}</option>)
                                    })}
                                </select>
                            </Col>
                            <Col sm={5}>
                                <input className="form-control"
                                       type="text"
                                       name="search"
                                       id="search"
                                       placeholder={ translate('interface.search') + '...' }
                                       onChange={this.handleChange}
                                />
                            </Col>

                        </Row>
                    </Col>
                    <Col lg={6}>
                        <Row className="form_no-padding">
                            <Col sm={2}>
                                <Button className="btn-xs btn-white btn-round "
                                        rel="tooltip"
                                        data-placement="bottom"
                                        title={ translate('interface.apply_filters') }
                                        onClick ={this.handleSubmitFilter}
                                >
                                    <i style={{ fontSize: '20px', padding: '5px' }} className="fa fa-filter" />
                                </Button>
                            </Col>
                            <Col sm={4}>
                                <select name="interest_id" onChange={this.handleChange} className="form-control">
                                    <option value={0}>{ translate('interface.interest') }: </option>
                                    {interests.length && interests.map(function (interest) {
                                        return (<option key={interest.id}
                                                        value={interest.id}>{interest[nameField]}</option>)
                                    })}
                                </select>
                            </Col>
                            <Col sm={4}>
                                <select name="source_id" className="form-control" onChange={this.handleChange}>
                                    <option value={0}>{ translate('interface.source') }: </option>
                                    {sources.length && sources.map(function (source) {
                                        return (<option key={source.id}
                                                        value={source.id}>{source[nameField]}</option>)
                                    })}
                                </select>
                            </Col>
                            <Col sm={2}>
                                <Button onClick={this.handleClear}
                                        className="btn-xs btn-white btn-round pull-right"
                                        rel="tooltip"
                                        data-placement="bottom"
                                        title={ translate('interface.clear_filters') }
                                >
                                    <i style={{ fontSize: '20px', padding: '5px' }} className="fa fa-close" />
                                </Button>
                            </Col>

                        </Row>
                    </Col>

                </Col>

            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        searchFields: state.reducer.init.searchFields,
        interests: state.reducer.init.interests,
        sources: state.reducer.init.sources,
        currentProject: state.reducer.currentProject,
        type: state.reducer.customers.type,
        active: state.reducer.customers.active,
        order_by: state.reducer.customers.order_by,
        order_direction: state.reducer.customers.order_direction
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getCustomersListAction: bindActionCreators(getCustomersListAction, dispatch),
        changeTypeAction: bindActionCreators(changeTypeAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Filters)