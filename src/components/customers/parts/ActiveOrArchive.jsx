import React, { Component } from 'react'
import { Button } from 'react-bootstrap'
import { hasRole } from '../../../utils/checkRoles'
import { showCustomersActiveModal, hideCustomersActiveModal } from "../../../actions/actionsCustomers"
import { bindActionCreators } from "redux"
import { connect } from "react-redux"

class ActiveOrArchive extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { customer, authUser, translate } = this.props

        if (hasRole(authUser, 'admin') || hasRole(authUser, 'manager')) {
            if (customer.active === 1) {
                return (
                    <Button type="button"
                            rel="tooltip"
                            title={ translate('interface.to_archive') }
                            onClick={() => this.props.showCustomersActiveModal({ action: 0, id: customer.id })}
                            className="fa fa-archive"
                    />
                )
            } else {
                return (
                <Button type="button"
                        rel="tooltip"
                        title={ translate('interface.to_active') }
                        onClick={() => this.props.showCustomersActiveModal({ action: 1, id: customer.id })}
                        className="fa fa-flash"
                />
                )
            }
        }
        return null
    }
}
function mapStateToProps(state) {
    return {
        modalActiveOrArchive: state.reducer.customers.modalActiveOrArchive
    }
}

function mapDispatchToProps(dispatch) {
    return {
        showCustomersActiveModal: bindActionCreators(showCustomersActiveModal, dispatch),
        hideCustomersActiveModal: bindActionCreators(hideCustomersActiveModal, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ActiveOrArchive)