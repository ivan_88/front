import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { bindActionCreators } from "redux"
import { getTranslate, getActiveLanguage  } from 'react-localize-redux'
import { createCustomerAction } from "../../actions/actionsCustomers"
import { setRedirectFalse } from "../../actions/actionsGlobal"
import { connect } from "react-redux"
import { Col, Row, Button } from 'react-bootstrap'
import Errors from "../parts/Errors"
import { getPart } from "../../utils/checkPart"

class CustomerCreate extends Component {

    constructor(props) {
        super(props)
        this.state = {
            firstname: '',
            lastname: '',
            phone: '',
            email: '',
            country_id: '',
            interest_id: '',
            source_id: '',
            contact_by: '',
            city: '',
            address: '',
            zip: '',
            note: '',
            project_id: '',
            app: getPart()
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.redirect) {
            this.props.setRedirectFalse()
            this.props.history.push('/' + getPart() + '/customers/list')
        }
    }

    handleSubmit(event) {
        event.preventDefault()
        this.state.project_id = this.props.currentProject.id
        this.props.createCustomerAction(this.state)
    }

    handleChange(event) {
        this.state[event.target.name] = event.target.value
    }

    render() {
        const { interests, sources, contacts, countries, currentLanguage, errors, translate } = this.props
        const nameField = currentLanguage !== undefined ? 'name_' + currentLanguage : 'name_en'

        return (
            <div className="content">
                <div className="container-fluid">
                    <Row>
                        <Col md={12}>
                            <div className="card card-plain">
                                <Row className="card-header"
                                     style={{backgroundImage: "linear-gradient(60deg, rgba(38, 198, 218, 0.6), rgba(0, 172, 193, 0.6))"}}>
                                    <Col xs={8}>
                                        <h4 className="title"> { translate('interface.create') } LEAD</h4>
                                        <p className="category">
                                            <Button onClick={this.props.history.goBack}
                                                    className="btn btn-sm btn-white static-color">
                                                { translate('interface.back') }
                                            </Button>
                                        </p>
                                    </Col>
                                </Row>

                                <Errors errors={errors} />

                                <div className="card-content">

                                    <form className="card-content">

                                        <Row>
                                            <Col md={3}>
                                                <div className="form-group label-floating">
                                                    <label className="control-label"> { translate('interface.firstname') }</label>
                                                    <input className="form-control"
                                                           type="text"
                                                           name="firstname"
                                                           onChange={this.handleChange}
                                                    />
                                                </div>
                                            </Col>
                                            <Col md={3}>
                                                <div className="form-group label-floating">
                                                    <label className="control-label"> { translate('interface.lastname') }</label>
                                                    <input className="form-control"
                                                           type="text"
                                                           name="lastname"
                                                           onChange={this.handleChange}
                                                    />
                                                </div>
                                            </Col>
                                            <Col md={3}>
                                                <div className="form-group label-floating">
                                                    <label className="control-label"> { translate('interface.phone') }</label>
                                                    <input className="form-control"
                                                           type="text"
                                                           name="phone"
                                                           onChange={this.handleChange}
                                                    />
                                                </div>
                                            </Col>
                                            <Col md={3}>
                                                <div className="form-group label-floating">
                                                    <label className="control-label"> { translate('interface.email') }</label>
                                                    <input className="form-control"
                                                           type="text"
                                                           name="email"
                                                           onChange={this.handleChange}
                                                    />
                                                </div>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={3}>
                                                <div className="form-group">
                                                    <label className="control-label"> { translate('interface.country') }</label>
                                                    <select name="country_id"
                                                            onChange={this.handleChange}
                                                            className="form-control autoselect"
                                                    >
                                                        { countries.length && countries.map(function (country) {
                                                            return (<option key={country.id}
                                                                            value={country.id}>{country.full_name}</option>)
                                                        })}

                                                    </select>
                                                </div>
                                            </Col>
                                            <Col md={3}>
                                                <div className="form-group">
                                                    <label className="control-label"> { translate('interface.interest') }</label>
                                                    <select name="interest_id" onChange={this.handleChange}
                                                            className="form-control">
                                                        <option value={0}></option>
                                                        { interests.length && interests.map(function (interest) {
                                                            return (<option key={interest.id}
                                                                            value={interest.id}>{interest[nameField]}</option>)
                                                        })}
                                                    </select>
                                                </div>
                                            </Col>
                                            <Col md={3}>
                                                <div className="form-group">
                                                    <label className="control-label"> { translate('interface.source') }</label>
                                                    <select name="source_id" className="form-control"
                                                            onChange={this.handleChange}>
                                                        <option value={0}></option>
                                                        {sources.length && sources.map(function (source) {
                                                            return (<option key={source.id}
                                                                            value={source.id}>{source[nameField]}</option>)
                                                        })}
                                                    </select>
                                                </div>
                                            </Col>
                                            <Col md={3}>
                                                <div className="form-group">
                                                    <label className="control-label"> { translate('interface.contact_by') }</label>
                                                    <select name="contact_by" className="form-control"
                                                            onChange={this.handleChange}>
                                                        <option value={0}></option>
                                                        {contacts.length && contacts.map(function (contact, key) {
                                                            return (<option key={key}
                                                                            value={contact.id}>{contact[currentLanguage]}</option>)
                                                        })}
                                                    </select>
                                                </div>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={3}>
                                                <div className="form-group label-floating">
                                                    <label className="control-label"> { translate('interface.city') }</label>
                                                    <input className="form-control"
                                                           type="text"
                                                           name="city"
                                                           onChange={this.handleChange}
                                                    />
                                                </div>
                                            </Col>
                                            <Col md={3}>
                                                <div className="form-group label-floating">
                                                    <label className="control-label"> { translate('interface.address') }</label>
                                                    <input className="form-control"
                                                           type="text"
                                                           name="address"
                                                           onChange={this.handleChange}
                                                    />
                                                </div>
                                            </Col>
                                            <Col md={3}>
                                                <div className="form-group label-floating">
                                                    <label className="control-label"> { translate('interface.zip') }</label>
                                                    <input className="form-control"
                                                           type="text"
                                                           name="zip"
                                                           onChange={this.handleChange}
                                                    />
                                                </div>
                                            </Col>
                                            <Row>
                                                <Col md={12}>
                                                    <div className="form-group">
                                                        <div className="form-group label-floating">
                                                            <label className="control-label"> { translate('interface.note') }</label>
                                                            <textarea className="form-control" rows="3" name="note"
                                                                      onChange={this.handleChange}>

                                                                        </textarea>
                                                        </div>
                                                    </div>
                                                </Col>
                                            </Row>
                                            <Button onClick={this.handleSubmit}> { translate('interface.create') }</Button>

                                            <div className="clearfix" />
                                        </Row>
                                    </form>
                                </div>
                            </div>
                        </Col>

                    </Row>
                </div>
            </div>
        )

    }
}
function mapStateToProps(state) {
    return {
        interests: state.reducer.init.interests,
        sources: state.reducer.init.sources,
        contacts: state.reducer.init.contacts,
        countries: state.reducer.init.countries,
        currentProject: state.reducer.currentProject,
        errors: state.reducer.customers.errors,
        redirect: state.reducer.redirect,
        translate: getTranslate(state.locale),
        currentLanguage: getActiveLanguage(state.locale).code
    }
}

function mapDispatchToProps(dispatch) {
    return {
        createCustomerAction: bindActionCreators(createCustomerAction, dispatch),
        setRedirectFalse: bindActionCreators(setRedirectFalse, dispatch)
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CustomerCreate))