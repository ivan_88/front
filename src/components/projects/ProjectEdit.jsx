import React, { Component } from 'react';
import { Col, Row, Label, Button } from 'react-bootstrap'
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { getTranslate } from 'react-localize-redux'
import { setRedirectFalse } from '../../actions/actionsGlobal'
import { updateProjectsAction, getProjectsAction } from '../../actions/actionsProjects'
import Errors from "../parts/Errors"
import Loader from '../parts/Loader'
import { getPart } from "../../utils/checkPart"

class ProjectEdit extends Component {
    constructor(props) {
        super(props)
        this.state = {
           project: {
               name_en: '',
               name_uk: ''
           }
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    handleSubmit(event) {
        event.preventDefault()
        this.props.updateProjectsAction(this.props.match.params.id, this.state.project)
    }

    handleChange(propertyName, event) {
        const project = this.state.project
        project[propertyName] = event.target.value
        this.setState({ project: project })
    }

    componentWillReceiveProps(nextProps) {
        this.state.project = nextProps.project
        if (nextProps.redirect) {
            this.props.setRedirectFalse()
            this.props.history.push('/' + getPart() + '/projects/list')
        }
    }

    componentDidMount() {
        this.props.getProjectsAction(this.props.match.params.id)
    }

    render() {
        const { project, errors, translate } = this.props

        if (project) {
            return (
                <div className="content">
                    <div className="container-fluid">
                        <Row>
                            <Col md={12}>
                                <div className="card card-plain">
                                    <Row className="card-header"
                                         style={{backgroundImage: "linear-gradient(60deg, rgba(38, 198, 218, 0.6), rgba(0, 172, 193, 0.6))"}}>
                                        <Col xs={8}>
                                            <h4 className="title">{ translate('interface.edit') } { project.name_en } - { project.name_uk } </h4>
                                            <p className="category">
                                                <Button onClick={this.props.history.goBack}
                                                        className="btn btn-sm btn-white static-color">
                                                    { translate('interface.back') }
                                                </Button>
                                            </p>
                                        </Col>
                                    </Row>

                                    <Errors errors={errors} />
                                    <Loader />

                                    <div className="card-content">
                                        <Row>
                                            <Col md={6}>
                                                <div className="form-group label-floating">
                                                    <Label className="control-label">{ translate('interface.name_en') }</Label>
                                                    <input className="form-control"
                                                           type="text"
                                                           name="name_en"
                                                           value={project.name_en}
                                                           onChange={this.handleChange.bind(this, 'name_en')}
                                                    />
                                                </div>
                                            </Col>
                                            <Col md={6}>
                                                <div className="form-group label-floating">
                                                    <Label className="control-label">{ translate('interface.name_uk') }</Label>
                                                    <input className="form-control"
                                                           type="text"
                                                           name="name_uk"
                                                           value={project.name_uk}
                                                           onChange={this.handleChange.bind(this, 'name_uk')}
                                                    />
                                                </div>
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={6}>
                                                <Button onClick={this.handleSubmit}>{ translate('interface.save') }</Button>
                                            </Col>
                                        </Row>

                                    </div>

                                </div>
                            </Col>
                        </Row>
                    </div>
                </div>
            )
        } else {
            return null
        }

    }
}

function mapStateToProps(state) {
    return {
        project: state.reducer.project,
        errors: state.reducer.projects.errors,
        redirect: state.reducer.redirect,
        translate: getTranslate(state.locale)
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateProjectsAction: bindActionCreators(updateProjectsAction, dispatch),
        setRedirectFalse: bindActionCreators(setRedirectFalse, dispatch),
        getProjectsAction: bindActionCreators(getProjectsAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectEdit)