import React, { Component } from 'react';
import { Col, Row, Table, Button, Alert } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { getTranslate } from 'react-localize-redux'
import { getProjectsListAction, deleteProjectsAction } from '../../actions/actionsProjects'
import Loader from '../parts/Loader'
import {getPart} from "../../utils/checkPart";

class ProjectsList extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.props.getProjectsListAction()
    }

    render() {
        const { projects, deleteProjectsAction, translate } = this.props

        return (
            <div className="content">
                <div className="container-fluid">
                    <Row>
                        <Col md={12}>
                            <div className="card card-plain">

                                <Row className="card-header"
                                     style={{
                                         backgroundImage: "linear-gradient(60deg, rgba(38, 198, 218, 0.6), rgba(0, 172, 193, 0.6))",
                                         paddingBottom: '20px'
                                     }}>
                                    <Col xs={6}>
                                        <h4 className="title">
                                            { translate('interface.projects') }
                                        </h4>
                                        <Link to={`/${getPart()}/projects/create`} className="btn btn-default">
                                            <i className={`fa fa-plus-circle`} />&nbsp;
                                            { translate('interface.create') }
                                        </Link>
                                    </Col>
                                </Row>

                                <div className="card-content table-responsive">
                                    { (projects && projects.length > 0) &&
                                        <Table className="table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>{ translate('interface.name_en') }</th>
                                                <th>{ translate('interface.name_uk') }</th>
                                                <th>{ translate('interface.actions') }</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            {projects.map(function (project) {
                                                return (
                                                    <tr key={project.id}>
                                                        <td>{project.name_en}</td>
                                                        <td>{project.name_uk}</td>
                                                        <td className="td-actions">

                                                            <Link to={`/${getPart()}/projects/edit/${project.id}`}
                                                                  type="button"
                                                                  rel="tooltip"
                                                                  title= { translate('interface.edit') }
                                                                  className="btn btn-default fa fa-edit"
                                                                  />
                                                            <Button onClick={() => deleteProjectsAction(project.id)}
                                                                    rel="tooltip"
                                                                    title= { translate('interface.delete') }
                                                                    className="btn btn-default fa fa-archive"
                                                            />
                                                        </td>
                                                    </tr>
                                                )
                                            })}
                                            </tbody>

                                        </Table>
                                    }
                                    <Loader />
                                </div>
                            </div>

                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state, history) {
    return {
        projects: state.reducer.projects.list,
        history: history,
        translate: getTranslate(state.locale)
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getProjectsListAction: bindActionCreators(getProjectsListAction, dispatch),
        deleteProjectsAction: bindActionCreators(deleteProjectsAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectsList)