import React from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Layout from './Layout'
import CustomersList from "./customers/CustomersList"
import CustomerCreate from "./customers/CustomerCreate"
import LogByCustomer from './logs/LogByCustomer'
import Login from "./auth/Login"
import Applications from './auth/Applications'
import CustomerEdit from "./customers/CustomerEdit"
import UsersList from './users/UsersList'
import UserCreate from "./users/UserCreate"
import UserEdit from "./users/UserEdit"
import SelfEdit from "./users/SelfEdit"
import PasswordChange from "./users/PasswordChange"
import LogByUser from "./logs/LogByUser"
import GlobalLog from './logs/GlobalLog'
import InterestsList from './interests/InterestsList'
import InterestCreate from './interests/InterestCreate'
import InterestEdit from './interests/InterestEdit'
import ProjectsList from "./projects/ProjectsList"
import ProjectCreate from "./projects/ProjectCreate"
import ProjectEdit from "./projects/ProjectEdit"
import RemindersList from './reminders/RemindersList'
import ReminderCreate from './reminders/ReminderCreate'
import ReminderEdit from './reminders/ReminderEdit'
import UsersPartnersList from './partners/UsersPartnersList'
import UserPartnerShow from  './partners/UserPartnerShow'
import UserPartnerCreate from './partners/UserPartnerCreate'

const Root = ({ store, history }) => (
    <div>
    <Provider store={store} history={history}>
        <Router>
            <Layout store={store} history={history}>
                <Route path="/brokers/customers/list" component={CustomersList} />
                <Route path="/brokers/customers/create" component={CustomerCreate} />
                <Route path="/brokers/customers/edit/:id" component={CustomerEdit} />
                <Route path="/brokers/customers/log/:id" component={LogByCustomer} />

                <Route path="/brokers/users/list" component={UsersList} />
                <Route path="/brokers/users/create" component={UserCreate} />
                <Route path="/brokers/users/edit/:id" component={UserEdit} />
                <Route path="/brokers/users/self" component={SelfEdit} />
                <Route path="/brokers/users/password" component={PasswordChange} />
                <Route path="/brokers/users/log/:id" component={LogByUser} />

                <Route path="/brokers/interests/list" component={InterestsList} />
                <Route path="/brokers/interests/create" component={InterestCreate} />
                <Route path="/brokers/interests/edit/:id" component={InterestEdit} />

                <Route path="/brokers/projects/list" component={ProjectsList} />
                <Route path="/brokers/projects/create" component={ProjectCreate} />
                <Route path="/brokers/projects/edit/:id" component={ProjectEdit} />

                <Route path="/brokers/reminders/list" component={RemindersList} />
                <Route path="/brokers/reminders/create" component={ReminderCreate} />
                <Route path="/brokers/reminders/edit/:id" component={ReminderEdit} />

                <Route path="/brokers/log" component={GlobalLog} />

                <Route exact path="/" component={Login} />
                <Route path="/applications" component={Applications} />

                {/*Костыль*/}
                <Route path="/sales/customers/list" component={CustomersList} />
                <Route path="/sales/customers/create" component={CustomerCreate} />
                <Route path="/sales/customers/edit/:id" component={CustomerEdit} />
                <Route path="/sales/customers/log/:id" component={LogByCustomer} />

                <Route path="/sales/users/list" component={UsersList} />
                <Route path="/sales/users/create" component={UserCreate} />
                <Route path="/sales/users/edit/:id" component={UserEdit} />
                <Route path="/sales/users/self" component={SelfEdit} />
                <Route path="/sales/users/password" component={PasswordChange} />
                <Route path="/sales/users/log/:id" component={LogByUser} />

                <Route path="/sales/interests/list" component={InterestsList} />
                <Route path="/sales/interests/create" component={InterestCreate} />
                <Route path="/sales/interests/edit/:id" component={InterestEdit} />

                <Route path="/sales/projects/list" component={ProjectsList} />
                <Route path="/sales/projects/create" component={ProjectCreate} />
                <Route path="/sales/projects/edit/:id" component={ProjectEdit} />

                <Route path="/sales/reminders/list" component={RemindersList} />
                <Route path="/sales/reminders/create" component={ReminderCreate} />
                <Route path="/sales/reminders/edit/:id" component={ReminderEdit} />

                <Route path="/sales/log" component={GlobalLog} />
                {/**/}

                <Route exact path="/partners/users/list" component={UsersPartnersList} />
                <Route exact path="/partners/users/show/:id" component={UserPartnerShow} />
                <Route path="/partners/users/create" component={UserPartnerCreate} />
                <Route path="/partners/users/edit/:id" component={UserEdit} />

                <Route path="/partners/customers/list" component={CustomersList} />
                <Route path="/partners/customers/create" component={CustomerCreate} />
                <Route path="/partners/customers/edit/:id" component={CustomerEdit} />
            </Layout>
        </Router>
    </Provider>
    </div>
)



Root.propTypes = {
    store: PropTypes.object.isRequired
}

export default Root