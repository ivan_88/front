import React, { Component } from 'react'
import { changeLanguageAction } from "../actions/actionsGlobal"
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { hasRole } from "../utils/checkRoles"
import { Link } from 'react-router-dom'
import { Label } from 'react-bootstrap'
import { setActiveLanguage, getLanguages } from 'react-localize-redux'
import { getActiveLanguage, getTranslate } from "react-localize-redux/lib/index"
import { getTodayRemindersListAction, showReminderModalAction, getReminderAction } from '../actions/actionsReminders'
import { getAuthUserAction } from "../actions/actionsAuth"
import ReminderModal from './reminders/ReminderModal'
import { getPart } from "../utils/checkPart"

class UpNavbar extends Component {
    constructor(props) {
        super(props)
    }

    logout() {
        localStorage.setItem('token', null)
        window.location.replace('/')
    }

    componentDidMount() {
        const getTodayRemindersListAction = this.props.getTodayRemindersListAction
        getTodayRemindersListAction()
        // TODO: Возможно стоит использовать веб-сокет
        window.setInterval(getTodayRemindersListAction, 10000)
        if (Object.keys(this.props.authUser).length === 0) {
            this.props.getAuthUserAction()
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.refresh) {
            this.props.getTodayRemindersListAction()
        }
    }

    render() {
        const { authUser, setActiveLanguage, languages, translate, remindersCount, remindersList, getReminderAction, showReminderModalAction } = this.props

        return (
            <nav className="up-navbar">
                <ReminderModal />
                <ul>
                    <li className="dropdown">
                        <span className="pseudolink">{ translate('interface.language') }</span>

                        <ul className="dropdown-menu">
                            {languages.map(language =>
                                <li key={language.code}>
                                    <span className="pseudolink" onClick={() => setActiveLanguage(language.code)}>
                                    {language.code}
                                    </span>
                                </li>
                            )}
                        </ul>
                    </li>

                    <li className="dropdown">
                        <span className="dropdown-toggle"
                           data-toggle="dropdown">
                            <span className="pseudolink">
                                 <i style={{ fontSize: '20px' }} className="fa fa-user-circle" />&nbsp;
                                { authUser.name }
                            </span>

                        </span>

                        <ul className="dropdown-menu">
                            <li><Link style={{ padding: '10px' }} to={`/${getPart()}/users/self`}>{ translate('interface.edit_account') }</Link></li>
                            <li><Link style={{ padding: '10px' }} to={`/${getPart()}/users/password`}>{ translate('interface.change_password') }</Link></li>
                            { hasRole(authUser, 'admin') &&
                                <li><Link style={{ padding: '10px' }} to={`/${getPart()}/projects/list`}>{ translate('interface.projects') }</Link></li>
                            }

                            { hasRole(authUser, 'admin') &&
                                <li><Link style={{ padding: '10px' }} to={`/${getPart()}/interests/list`}>{ translate('interface.interests') }</Link></li>
                            }
                            <li><Link style={{ padding: '10px', cursor: 'pointer' }} to={'/applications'}>{ translate('interface.apps') }</Link></li>
                            <li><a style={{ padding: '10px', cursor: 'pointer' }} onClick={() => this.logout()}>{ translate('interface.logout') }</a></li>
                        </ul>
                    </li>

                    <li className="dropdown">
                        <span className="pseudolink">
                            <i style={{ fontSize: '20px' }} className="fa fa-clock-o" />&nbsp;
                            <Label>{ remindersCount }</Label>
                        </span>
                        <ul className="dropdown-menu">
                            {remindersList && remindersList.length > 0 ?
                                remindersList.map(function (reminder) {
                                    return (
                                        <li key={reminder.id}>
                                            <span className="pseudolink" style={{ width: '200px' }}
                                                  onClick={function () {
                                                      getReminderAction(reminder.id)
                                                      showReminderModalAction(reminder.id)
                                                  }}>
                                                {reminder.note.substr(0, 25)}...
                                                </span>
                                        </li>
                                    )
                                })
                                :
                                <li>
                                        <span className="pseudolinc">
                                            NONE
                                        </span>
                                </li>
                            }
                        </ul>
                    </li>
                </ul>
            </nav>
        )
    }
}
function mapStateToProps(state) {
    return {
        authUser: state.reducer.authUser,
        languages: getLanguages(state.locale),
        translate: getTranslate(state.locale),
        currentLanguage: getActiveLanguage(state.locale).code,
        remindersCount: state.reducer.todayReminders.count,
        remindersList: state.reducer.todayReminders.list,
        refresh: state.reducer.todayReminders.refresh
    }
}

function mapDispatchToProps(dispatch) {
    return {
        changeLanguageAction: bindActionCreators(changeLanguageAction, dispatch),
        setActiveLanguage: bindActionCreators(setActiveLanguage, dispatch),
        getTodayRemindersListAction: bindActionCreators(getTodayRemindersListAction, dispatch),
        showReminderModalAction: bindActionCreators(showReminderModalAction, dispatch),
        getReminderAction: bindActionCreators(getReminderAction, dispatch),
        getAuthUserAction: bindActionCreators(getAuthUserAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpNavbar)