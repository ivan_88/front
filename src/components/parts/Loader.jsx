import React, { Component } from 'react'
import {connect} from "react-redux";

class Loader extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        if (this.props.loading) {
            return (
                <div id="loading" />
            )
        }
        return null
    }
}
function mapStateToProps(state) {
    return {
        loading: state.reducer.loading
    }
}

export default connect(mapStateToProps)(Loader)