import React, { Component } from 'react'
import { Alert } from 'react-bootstrap'

export default class Errors extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const errors = this.props.errors
        if ( typeof errors === 'object' && Object.keys(errors).length > 0) {
            return (
                <Alert style={{ margin: '10px' }} className='alert-danger'>
                    <ul>
                        {Object.keys(errors).map(function (error) {
                            return (errors[error].map(function (msg, i) {
                                return (<li key={i}>{msg}</li>)
                            }))
                        })}
                    </ul>
                </Alert>
            )
        }
        return null
    }
}