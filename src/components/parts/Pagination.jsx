import React, { Component } from "react"
import { Button } from 'react-bootstrap'
import { getCustomersListAction, getNotesListAction } from "../../actions/actionsCustomers"
import { getLogsByCustomerAction, getLogsByUserAction, getAllLogsAction } from '../../actions/actionsLogs'
import { getRemindersListAction } from '../../actions/actionsReminders'
import { bindActionCreators } from "redux"
import { connect } from "react-redux"

class Pagination extends Component {
    constructor(props) {
        super(props)
    }

    addLi(count, build, params) {
        let action = this.props[this.props.action]
        let currentPage = build.length + 1
        let active = params.thisPage === currentPage ? 'active' : ''

        build.push(<li key={currentPage} style={{ listStyleType: 'none', display: 'inline-block' }}>
             <Button className={`${active}`} onClick={() => action({
                 page: currentPage,
                 active: params.active ? params.active : null,
                 type: params.type ? params.type : null,
                 project: params.projectId ? params.projectId : null,
                 id: params.id ? params.id : null,
                 order_by: params.order_by ? params.order_by : null,
                 order_direction: params.order_by ? params.order_direction : null,
                 today: typeof params.today !== undefined ? params.today : null,
                 executed: typeof params.executed !== undefined ? params.executed : null,
                 start: params.start ? params.start : null,
                 end: params.end ? params.end : null
             })
             }>
                 { currentPage }
             </Button>
         </li>)
        if (currentPage < count) {
            this.addLi(count, build, params)
        }
        return build
    }

    render() {
        const { page, count, active, type, projectId, id, order_by, order_direction, today, executed, start, end } = this.props
        const links = this.addLi(count, [], { thisPage: page, active, type, projectId, id, order_by, order_direction, today, executed, start, end })

        if (links.length > 1) {
            return (
                <ul>
                    { links.map(link => {
                        return link
                    }) }
                </ul>
            )
        }
        return null
    }
}

function mapStateToProps(state) {
    return {}
}

function mapDispatchToProps(dispatch) {
    return {
        getCustomersListAction: bindActionCreators(getCustomersListAction, dispatch),
        getLogsByCustomerAction: bindActionCreators(getLogsByCustomerAction, dispatch),
        getLogsByUserAction: bindActionCreators(getLogsByUserAction, dispatch),
        getNotesListAction: bindActionCreators(getNotesListAction, dispatch),
        getAllLogsAction: bindActionCreators(getAllLogsAction, dispatch),
        getRemindersListAction: bindActionCreators(getRemindersListAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Pagination)