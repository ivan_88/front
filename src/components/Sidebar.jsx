import React, {Component} from 'react'
import { NavLink } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Label } from 'react-bootstrap'
import { changeProjectAction, changeTypeAction, getCustomersListAction } from "../actions/actionsCustomers"
import { initAction, getTranslatesAction } from '../actions/actionsGlobal'
import { hasRole } from "../utils/checkRoles"
import { getTranslate, getActiveLanguage } from 'react-localize-redux'
import {checkPart, getPart} from "../utils/checkPart"

class Sidebar extends Component {
    constructor(props) {
        super(props)
        this.changeType = this.changeType.bind(this)
    }

    componentDidMount() {
        this.props.initAction()
    }

    changeType(type) {
        this.props.changeTypeAction(type)
        this.props.getCustomersListAction({ project: this.props.currentProject.id, type: type, active: this.props.active, app: getPart() })
    }

    render() {
        const { projects, types, authUser, translate, currentLanguage, remindersCount, changeProjectAction } = this.props
        let { currentProject, type } = this.props
        if (typeof currentProject === 'undefined') {
            currentProject = projects[0]
        }
        if (typeof type === 'undefined') {
            type = 1
        }

        const nameField = currentLanguage !== undefined ? 'name_' + currentLanguage : 'name_en'

        if (projects === undefined || currentLanguage === undefined || types === undefined || type === undefined || currentProject === undefined ) {
            return (
                <div className="sidebar" data-color="red" data-image="http://belgianvillage.com/images/pustomyty.jpg">
                    <h1>Loading...</h1>
                </div>
            )
        }

        return (

            <div className="sidebar">

                <div className="logo" style={{ textAlign: 'center' }} >
                    <NavLink className="simple-text" to={`/${getPart()}/customers/list`}  >
                        <img src="/images/bv-crm-logo.svg" alt="CRM" style={{ height: '64px' }} />
                    </NavLink>
                    <h3 className="text-center">
                        {currentProject && currentProject[nameField]}
                    </h3>
                    <div style={{ borderBottom: '1px solid #ddd', margin: '30px' }} >


                    </div>
                </div>

                <div className="sidebar-wrapper">
                    <ul className="nav">

                        <li className="projects">
                            <a href="#" className="dropdown-toggle " data-toggle="dropdown">
                                <i style={{ fontSize: '20px' }} className="fa fa-star" />&nbsp;
                                <span>{ translate('interface.projects') }</span>
                            </a>
                            <ul className="projects-list">
                                { (projects && projects.length > 0) &&
                                    projects.map(function (project, i) {
                                        return <li style={{ color: '#89949B', cursor: 'pointer' }}
                                                   key={i}
                                                   onClick={() => changeProjectAction(project)}>
                                            { project[nameField] }
                                        </li>
                                    })
                                }
                            </ul>
                        </li>

                        { ((hasRole(authUser, 'admin') || hasRole(authUser, 'manager')) && (checkPart('brokers') || checkPart('sales'))) &&
                            <li>
                                <NavLink to={`/${getPart()}/users/list`}>
                                    <i style={{ fontSize: '20px' }} className="fa fa-user-circle" />&nbsp;
                                    <span>{ translate('interface.users') }</span>
                                </NavLink>
                            </li>
                        }

                        { checkPart('partners') &&
                            <li>
                                <NavLink to={`/${getPart()}/users/list`}>
                                    <i style={{ fontSize: '20px' }} className="fa fa-user-circle" />&nbsp;
                                    <span>{ translate('interface.referrals') }</span>
                                </NavLink>
                            </li>
                        }

                        { (hasRole(authUser, 'admin') && (checkPart('brokers') || checkPart('sales'))) &&
                            <li>
                                <NavLink to={`/${getPart()}/log`}>
                                    <i style={{ fontSize: '20px' }} className="fa fa-history" />&nbsp;
                                    <span>{ translate('interface.log') }</span>
                                </NavLink>
                            </li>
                        }

                        { (checkPart('brokers') || checkPart('sales')) &&
                            <li>
                                <NavLink to={`/${getPart()}/reminders/list`}>
                                    <i style={{ fontSize: '20px' }} className="fa fa-clock-o" />&nbsp;
                                    <span>
                                    { translate('interface.reminders') }&nbsp;
                                        <Label>
                                        { remindersCount }
                                    </Label>
                                </span>
                                </NavLink>
                            </li>
                        }

                        <li>
                            <NavLink to={`/${getPart()}/customers/list`}  onClick={() => this.changeType(1)}>
                                <i className="material-icons material-icon_success">assignment_returned</i>&nbsp;
                                <span>LEAD</span>
                            </NavLink>
                        </li>
                        { (checkPart('brokers') || checkPart('sales')) &&
                        <li>
                            <NavLink to={`/${getPart()}/customers/list`} onClick={() => this.changeType(2)}>
                                <i className="material-icons material-icon_warning">assignment_returned</i>&nbsp;
                                <span>COM</span>
                            </NavLink>
                        </li>
                        }
                        { (checkPart('brokers') || checkPart('sales')) &&
                        <li>
                            <NavLink to={`/${getPart()}/customers/list`} onClick={() => this.changeType(3)}>
                                <i className="material-icons material-icon_info">assignment_returned</i>&nbsp;
                                <span>NEG</span>
                            </NavLink>
                        </li>
                        }
                        { (checkPart('brokers') || checkPart('sales')) &&
                        <li>
                            <NavLink to={`/${getPart()}/customers/list`} onClick={() => this.changeType(4)}>
                                <i className="material-icons material-icon_danger">assignment_returned</i>&nbsp;
                                <span>PRO</span>
                            </NavLink>
                        </li>
                        }

                    </ul>

                </div>

            </div>
        );
    }
}

function mapStateToProps(state, history) {
    return {
        types: state.reducer.init.types,
        projects: state.reducer.init.projects,
        currentProject: state.reducer.currentProject,
        active: state.reducer.customers.active,
        countries: state.reducer.init.countries,
        interests: state.reducer.init.interests,
        sources: state.reducer.init.sources,
        contacts: state.reducer.init.contacts,
        roles: state.reducer.init.roles,
        actions: state.reducer.init.actions,
        authUser: state.reducer.authUser,
        translates: state.reducer.translates,
        history: history.history,
        translate: getTranslate(state.locale),
        currentLanguage: getActiveLanguage(state.locale).code,
        remindersCount: state.reducer.todayReminders.count,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        initAction: bindActionCreators(initAction, dispatch),
        changeProjectAction: bindActionCreators(changeProjectAction, dispatch),
        changeTypeAction: bindActionCreators(changeTypeAction, dispatch),
        getTranslatesAction: bindActionCreators(getTranslatesAction, dispatch),
        getCustomersListAction: bindActionCreators(getCustomersListAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar)



