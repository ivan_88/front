import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { bindActionCreators } from "redux"
import { getTranslate, getActiveLanguage  } from 'react-localize-redux'
import { getAllLogsAction, getAllLogsExcelAction } from "../../actions/actionsLogs"
import { connect } from "react-redux"
import { Col, Row, Table, Button, Label } from 'react-bootstrap'
import Datetime from 'react-datetime'
import moment from 'moment'
import Pagination from "../parts/Pagination"
import Loader from '../parts/Loader'

class GlobalLog extends Component {
    constructor(props) {
        super(props)
        this.state = {
            logs: [],
            start: null,
            end: null
        }

        this.handleChangeStart = this.handleChangeStart.bind(this)
        this.handleChangeEnd = this.handleChangeEnd.bind(this)
        this.handleFilter = this.handleFilter.bind(this)
        this.handleClear = this.handleClear.bind(this)
    }

    handleChangeStart(date) {
        this.setState({ start: date })
    }

    handleChangeEnd(date) {
        this.setState({ end: date })
    }

    handleFilter() {
        this.props.getAllLogsAction({
            start: this.state.start ? this.state.start.format("YYYY-MM-DD HH:mm:ss") : null,
            end: this.state.end ? this.state.end.format("YYYY-MM-DD HH:mm:ss") : null
        })
    }

    handleClear() {
        this.setState({ start: null, end: null })
        this.props.getAllLogsAction({ start: null, end: null})
    }

    componentDidMount() {
        this.props.getAllLogsAction()
    }

    render() {
        const { logs, actions, page, total, perPage, translate, currentLanguage } = this.props
        const { start, end } = this.state

        return (
            <div className="content">
                <div className="container-fluid">
                    <Row>
                        <Col md={12}>
                            <div className="card card-plain">
                                <Row className="card-header" style={{
                                    backgroundImage: "linear-gradient(60deg, rgba(38, 198, 218, 0.6), rgba(0, 172, 193, 0.6))",
                                    paddingBottom: '10px',
                                    marginBottom: '20px'
                                }}>
                                    <Col xs={8}>
                                        <h4 className="title">{ translate('interface.global_log') }</h4>
                                      
                                    </Col>
                                    <Col xs={8}>
                                        <Button onClick={() => this.props.getAllLogsExcelAction({
                                            start: this.state.start ? this.state.start.format("YYYY-MM-DD HH:mm:ss") : null,
                                            end: this.state.end ? this.state.end.format("YYYY-MM-DD HH:mm:ss") : null,
                                            excel: 1
                                        }, `Report: All.xls`)}
                                                className="btn btn-sm btn-white static-color">
                                            { translate('interface.get_excel') }
                                        </Button>
                                        </Col>
                                </Row>
                                <Row>
                                    <Col lg={6}>
                                        <Row>
                                            <Col md={5}>
                                                <Label className="control-label"> { translate('interface.start') }</Label>
                                                <Datetime
                                                    selected={this.state.start}
                                                    onChange={this.handleChangeStart}
                                                    dateFormat="YYYY-MM-DD"
                                                    timeFormat="HH:mm:ss"
                                                    size="md"
                                                    locale={currentLanguage}
                                                />
                                            </Col>
                                            <Col md={5}>
                                                <Label className="control-label"> { translate('interface.end') }</Label>
                                                <Datetime
                                                    selected={this.state.end}
                                                    onChange={this.handleChangeEnd}
                                                    dateFormat="YYYY-MM-DD"
                                                    timeFormat="HH:mm:ss"
                                                    size="md"
                                                    locale={currentLanguage}
                                                />
                                            </Col>
                                            <Col md={1}>
                                                <Button onClick={this.handleFilter}
                                                        className="btn btn-default fa fa-filter"
                                                        rel="tooltip"
                                                        title={ translate('interface.filter') }
                                                        style={{ marginTop: '20px', fontSize: '20px', padding: '5px 10px' }} />
                                            </Col>
                                            <Col md={1}>
                                                <Button onClick={this.handleClear}
                                                        className="btn btn-default fa fa-close"
                                                        rel="tooltip"
                                                        title={ translate('interface.clear_filters') }
                                                        style={{ marginTop: '20px', fontSize: '20px', padding: '5px 10px' }}/>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                                <div className="card-content table-responsive">

                                    <Table className="table table-hover">
                                        <thead>
                                        <tr>
                                            <th className="text-center"><b>{ translate('interface.user') }</b></th>
                                            <th className="text-center"><b>{ translate('interface.customer') }</b></th>
                                            <th className="text-center"><b>{ translate('interface.action') }</b></th>
                                            <th className="text-center"><b>{ translate('interface.date') }</b></th>
                                        </tr>
                                        </thead>
                                        { (logs.length > 0 && Object.keys(actions).length > 0) &&
                                            <tbody>
                                            { logs.map(function (row) {
                                                return (
                                                    <tr key={row.id}>
                                                        <td className="text-center">
                                                            { row.user.name }</td>
                                                        <td className="text-center">
                                                            {row.customer.firstname} {row.customer.lastname}</td>
                                                        <td className="text-center">
                                                            { translate('log.' + actions[row.action]) }
                                                            { row.action == 3 ? <span>:&nbsp;<i>{ row.customer.rejection_reason }</i></span> : null }
                                                            { row.action == 4 ? <span>:&nbsp;<i>{ row.customer.cause_of_recovery }</i></span> : null }
                                                            </td>
                                                        <td className="text-center">{moment(row.created_at).format('DD-MM-YYYY HH:mm')}</td>
                                                    </tr>
                                                )
                                            })}
                                            </tbody>
                                        }
                                    </Table>
                                </div>
                                <Loader />
                                <Pagination page={page}
                                            count={Math.ceil(total / perPage)}
                                            action={'getAllLogsAction'}
                                            start={start ? start.format("YYYY-MM-DD HH:mm:ss") : null}
                                            end={end ? end.format("YYYY-MM-DD HH:mm:ss") : null}
                                />
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        )

    }
}
function mapStateToProps(state) {
    return {
        logs: state.reducer.logsAll.list,
        actions: state.reducer.logsAll.actions,
        translates: state.reducer.translates,
        page: state.reducer.logsAll.page,
        perPage: state.reducer.logsAll.perPage,
        total: state.reducer.logsAll.total,
        translate: getTranslate(state.locale),
        currentLanguage: getActiveLanguage(state.locale).code
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getAllLogsAction: bindActionCreators(getAllLogsAction, dispatch),
        getAllLogsExcelAction: bindActionCreators(getAllLogsExcelAction, dispatch)
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(GlobalLog))