import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { bindActionCreators } from "redux"
import { getTranslate, getActiveLanguage  } from 'react-localize-redux'
import { getLogsByUserAction, getLogsByUserExcelAction } from "../../actions/actionsLogs"
import { connect } from "react-redux"
import { Col, Row, Table, Button } from 'react-bootstrap'
import Pagination from "../parts/Pagination"
import Loader from '../parts/Loader'
import moment from "moment/moment";

class LogByUser extends Component {
    constructor(props) {
        super(props)
        this.state = {
            logs: [],
            user: {}
        }
    }

    componentDidMount() {
        this.props.getLogsByUserAction({ id: this.props.match.params.id })
    }

    render() {
        const { logs, user, actions, page, total, perPage, translate } = this.props
        const id = this.props.match.params.id

            return (
                <div className="content">
                    <div className="container-fluid">
                        <Row>
                            <Col md={12}>
                                <div className="card card-plain">
                                    <Row className="card-header" style={{ backgroundImage: "linear-gradient(60deg, rgba(38, 198, 218, 0.6), rgba(0, 172, 193, 0.6))" }}>
                                        <Col xs={8}>
                                            <h4 className="title">{ translate('interface.log') }: <b>{ user.name }</b></h4>
                                            <p className="category">
                                                <Button onClick={this.props.history.goBack}
                                                        className="btn btn-sm btn-white static-color">
                                                    { translate('interface.back') }
                                                </Button>
                                                <Button onClick={() => this.props.getLogsByUserExcelAction(user.id, `Report for user: ${user.name}.xls`)}
                                                        className="btn btn-sm btn-white static-color">
                                                    { translate('interface.get_excel') }
                                                </Button>
                                            </p>
                                        </Col>
                                    </Row>
                                    <div className="card-content table-responsive">
                                        <Table className="table table-hover">
                                            <thead>
                                            <tr>
                                                <th className="text-center"><b>{ translate('interface.customer') }</b></th>
                                                <th className="text-center"><b>{ translate('interface.action') }</b></th>
                                                <th className="text-center"><b>{ translate('interface.date') }</b></th>
                                            </tr>
                                            </thead>


                                            { (logs.length > 0 && Object.keys(actions).length > 0) &&
                                                <tbody>
                                                { logs.map(function (row) {
                                                    return (
                                                        <tr key={row.id}>
                                                            <td className="text-center">
                                                                {row.customer.firstname} {row.customer.lastname}
                                                            </td>
                                                            <td className="text-center">
                                                                { translate('log.' + actions[row.action]) }
                                                                { row.action == 3 ? <span>:&nbsp;<i>{ row.customer.rejection_reason }</i></span> : null }
                                                                { row.action == 4 ? <span>:&nbsp;<i>{ row.customer.cause_of_recovery }</i></span> : null }
                                                            </td>
                                                            <td className="text-center">{moment(row.created_at).format('DD-MM-YYYY HH:mm')}</td>
                                                        </tr>
                                                    )
                                                })

                                            }
                                                </tbody>
                                            }


                                        </Table>
                                    </div>
                                    <Loader />
                                    <Pagination page={page} count={Math.ceil(total / perPage)} id={id} action={'getLogsByUserAction'} />
                                </div>
                            </Col>
                        </Row>
                    </div>
                </div>
            )

    }
}
function mapStateToProps(state) {
    return {
        user: state.reducer.user,
        logs: state.reducer.logsByUser.list,
        actions: state.reducer.logsByUser.actions,
        translates: state.reducer.translates,
        page: state.reducer.logsByUser.page,
        perPage: state.reducer.logsByUser.perPage,
        total: state.reducer.logsByUser.total,
        translate: getTranslate(state.locale),
        currentLanguage: getActiveLanguage(state.locale).code
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getLogsByUserAction: bindActionCreators(getLogsByUserAction, dispatch),
        getLogsByUserExcelAction: bindActionCreators(getLogsByUserExcelAction, dispatch)
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LogByUser))