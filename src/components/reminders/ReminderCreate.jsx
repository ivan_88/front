import React, { Component } from 'react';
import { Col, Row, Label, Button } from 'react-bootstrap'
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { getTranslate } from 'react-localize-redux'
import { setRedirectFalse } from '../../actions/actionsGlobal'
import { createReminderAction, getCustomersListAction } from '../../actions/actionsReminders'
import Errors from "../parts/Errors"
import 'react-datepicker/dist/react-datepicker.css'
import Datetime from 'react-datetime'
import 'react-datepicker/dist/react-datepicker.css'
import { getActiveLanguage } from "react-localize-redux/lib/index"
import  { getPart } from "../../utils/checkPart"


class ReminderCreate extends Component {
    constructor(props) {
        super(props)
        this.state = {
            assigned_to: null,
            content_id: null,
            note: ''
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleChangeDate = this.handleChangeDate.bind(this)
    }

    handleSubmit(event) {
        event.preventDefault()
        this.props.createReminderAction({
            content_id: this.state.content_id,
            note: this.state.note,
            assigned_to: this.state.assigned_to.format("YYYY-MM-DD HH:mm:ss")
        })
    }

    handleChange(event) {
        this.state[event.target.name] = event.target.value
    }

    handleChangeDate(date) {
        this.setState({ assigned_to: date })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.redirect) {
            this.props.setRedirectFalse()
            this.props.history.push('/' + getPart() + '/reminders/list')
        }
    }

    componentDidMount() {
        this.props.getCustomersListAction()
    }

    render() {
        const { errors, translate, customers, currentLanguage } = this.props
        return (
            <div className="content">
                <div className="container-fluid">
                    <Row>
                        <Col md={12}>
                            <div className="card card-plain">
                                <Row className="card-header"
                                     style={{backgroundImage: "linear-gradient(60deg, rgba(38, 198, 218, 0.6), rgba(0, 172, 193, 0.6))"}}>
                                    <Col xs={8}>
                                        <h4 className="title">{ translate('interface.create_reminder') }</h4>
                                        <p className="category">
                                            <Button onClick={this.props.history.goBack}
                                                    className="btn btn-sm btn-white static-color">
                                                { translate('interface.back') }
                                            </Button>
                                        </p>
                                    </Col>
                                </Row>

                                <Errors errors={errors} />

                                <div className="card-content">
                                    <Row>
                                        <Col md={6}>
                                            <div className="form-group label-floating">
                                                <Label className="control-label">{ translate('interface.assigned_to') }</Label>
                                                <Datetime
                                                    onChange={this.handleChangeDate}
                                                    dateFormat="YYYY-MM-DD"
                                                    timeFormat="HH:mm:ss"
                                                    size="md"
                                                    locale={currentLanguage}
                                                />
                                            </div>
                                        </Col>
                                        <Col md={6}>
                                            <div className="form-group label-floating">
                                                <Label className="control-label">{ translate('interface.customer') }</Label>
                                                <select name="content_id" className="form-control" onChange={this.handleChange}>
                                                    <option value={0}>Choose...</option>
                                                    { (customers && customers.length > 0) && customers.map(function (customer) {
                                                        return (
                                                            <option key={customer.id} value={customer.id} >
                                                                { customer.firstname } { customer.lastname }
                                                            </option>
                                                        )
                                                    }) }
                                                </select>
                                            </div>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={12}>
                                            <div className="form-group label-floating">
                                                <Label className="control-label">{ translate('interface.note') }</Label>
                                                <textarea className="form-control"
                                                          rows="3"
                                                          name="note"
                                                          onChange={this.handleChange}>
                                                </textarea>
                                            </div>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={6}>
                                            <Button onClick={this.handleSubmit}>{ translate('interface.create') }</Button>
                                        </Col>
                                    </Row>

                                </div>

                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        errors: state.reducer.reminders.errors,
        redirect: state.reducer.redirect,
        customers: state.reducer.reminders.customersList,
        translate: getTranslate(state.locale),
        currentLanguage: getActiveLanguage(state.locale).code
    }
}

function mapDispatchToProps(dispatch) {
    return {
        createReminderAction: bindActionCreators(createReminderAction, dispatch),
        setRedirectFalse: bindActionCreators(setRedirectFalse, dispatch),
        getCustomersListAction: bindActionCreators(getCustomersListAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReminderCreate)