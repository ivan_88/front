import React, { Component } from 'react';
import { Col, Row, Table, Button, ButtonGroup } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { getTranslate } from 'react-localize-redux'
import { getRemindersListAction, deleteReminderAction, changeExecutedAction, changeTodayAction, doneAction } from '../../actions/actionsReminders'
import Loader from '../parts/Loader'
import Pagination from "../parts/Pagination"
import { getPart } from "../../utils/checkPart"

class RemindersList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            executed: 0,
            today: 0
        }
    }

    componentDidMount() {
        this.props.getRemindersListAction({ executed: this.state.executed, today: this.state.today })
    }

    componentWillReceiveProps (nextProps) {
        if (this.props.executed != nextProps.executed) {
            this.props.getRemindersListAction({ executed: nextProps.executed, today: this.props.today })
        }
        if (this.props.today != nextProps.today) {
            this.props.getRemindersListAction({ today: nextProps.today, executed: this.props.executed })
        }
    }

    render() {
        const { reminders, executed, today, page, perPage, total, deleteReminderAction, changeExecutedAction, changeTodayAction, doneAction, translate } = this.props

        return (
            <div className="content">
                <div className="container-fluid">
                    <Row>
                        <Col md={12}>
                            <div className="card card-plain">

                                <Row className="card-header"
                                     style={{backgroundImage: "linear-gradient(60deg, rgba(38, 198, 218, 0.6), rgba(0, 172, 193, 0.6))"}}>
                                    <Col xs={6}>
                                        <h4 className="title">
                                            { translate('interface.reminders') }:  {executed === 0 ? translate('interface.active') : translate('interface.executed')} -  {today === 1 ? translate('interface.today') : translate('interface.all_times')}
                                        </h4>
                                        <p className="category">
                                            <ButtonGroup>
                                                <Button
                                                    rel="tooltip"
                                                    onClick={() => changeExecutedAction(executed === 0 ? 1 : 0)}
                                                    title={ translate('interface.executed') }>
                                                    <i className={`fa fa-${today === 0 ? 'flash' : 'archive'}`} />&nbsp;
                                                    {executed === 1 ? translate('interface.active') : translate('interface.executed')}
                                                </Button>
                                                <Button
                                                    rel="tooltip"
                                                    onClick={() => changeTodayAction(today === 0 ? 1 : 0)}
                                                    title={ translate('interface.executed') }>
                                                    <i className={`fa fa-${today === 0 ? 'calendar-check-o' : 'calendar'}`} />&nbsp;
                                                    {today === 0 ? translate('interface.today') : translate('interface.all_times')}
                                                </Button>
                                                <Link to={`/${getPart()}/reminders/create`} className="btn btn-default">
                                                    <i className={`fa fa-plus-circle`} />&nbsp;
                                                    { translate('interface.create') }
                                                </Link>
                                            </ButtonGroup>
                                        </p>
                                    </Col>

                                    <Col md={6} className="text-right">

                                    </Col>
                                </Row>

                                <div className="card-content table-responsive">

                                        <Table className="table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>{ translate('interface.for_customer') }</th>
                                                <th>{ translate('interface.note') }</th>
                                                <th>{ translate('interface.assigned_to') }</th>
                                                <th>{ translate('interface.actions') }</th>
                                            </tr>
                                            </thead>
                                            { (reminders && reminders.length > 0) &&
                                            <tbody>
                                            {reminders.map(function (reminder) {
                                                return (
                                                    <tr key={reminder.id}>
                                                        <td>{reminder.id}</td>
                                                        <td>{reminder.content.firstname} {reminder.content.lastname}</td>
                                                        <td>{reminder.note}</td>
                                                        <td>{reminder.assigned_to}</td>
                                                        <td className="td-actions">

                                                            <Link to={`/${getPart()}/reminders/edit/${reminder.id}`}
                                                                  type="button"
                                                                  rel="tooltip"
                                                                  title= { translate('interface.edit') }
                                                                  className="btn btn-default fa fa-edit"
                                                            />
                                                            <Button onClick={() => deleteReminderAction(reminder.id)}
                                                                    rel="tooltip"
                                                                    title= { translate('interface.delete') }
                                                                    className="btn btn-default fa fa-archive"
                                                            />
                                                            <Button type="button"
                                                                    rel="tooltip"
                                                                    title={ executed === 0 ? translate('interface.execute') : translate('interface.return') }
                                                                    onClick={() => doneAction(reminder.id, { done: executed === 0 ? 1 : 0 })}
                                                                    className={`btn btn-default fa ${ executed === 0 ? 'fa-check' : 'fa-times' }`}
                                                            />
                                                        </td>
                                                    </tr>
                                                )
                                            })}
                                            </tbody>
                                            }
                                        </Table>

                                    <Loader />
                                    <Pagination page={page} count={Math.ceil(total / perPage)} action={'getRemindersListAction'} executed={executed} today={today} />
                                </div>
                            </div>

                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state, history) {

    return {
        reminders: state.reducer.reminders.list,
        executed: state.reducer.reminders.executed,
        today: state.reducer.reminders.today,
        page: state.reducer.reminders.page,
        perPage: state.reducer.reminders.perPage,
        total: state.reducer.reminders.total,
        history: history,
        translate: getTranslate(state.locale)
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getRemindersListAction: bindActionCreators(getRemindersListAction, dispatch),
        deleteReminderAction: bindActionCreators(deleteReminderAction, dispatch),
        changeExecutedAction: bindActionCreators(changeExecutedAction, dispatch),
        changeTodayAction: bindActionCreators(changeTodayAction, dispatch),
        doneAction: bindActionCreators(doneAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RemindersList)