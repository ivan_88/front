import React, { Component } from 'react';
import { Col, Row, Label, Button } from 'react-bootstrap'
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { getTranslate } from 'react-localize-redux'
import { setRedirectFalse } from '../../actions/actionsGlobal'
import { updateReminderAction, editReminderAction } from '../../actions/actionsReminders'
import moment from 'moment'
import { getActiveLanguage } from "react-localize-redux/lib/index"
import 'react-datepicker/dist/react-datepicker.css'
import Datetime from 'react-datetime'
import Errors from "../parts/Errors"
import Loader from '../parts/Loader'
import {getPart} from "../../utils/checkPart";


class ReminderEdit extends Component {
    constructor(props) {
        super(props)
        this.state = {
            reminder: {
                assigned_to: null,
                content_id: null,
                note: ''
            },
            assigned_to: null
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleChangeDate = this.handleChangeDate.bind(this)
    }

    handleSubmit(event) {
        event.preventDefault()
        this.props.updateReminderAction(this.props.match.params.id, {
            content_id: this.state.reminder.content_id,
            note: this.state.reminder.note,
            assigned_to: this.state.assigned_to.format("YYYY-MM-DD HH:mm:ss")
        })
    }

    handleChange(propertyName, event) {
        const reminder = this.state.reminder
        reminder[propertyName] = event.target.value
        this.setState({ reminder: reminder })
    }

    handleChangeDate(date) {
        this.setState({ assigned_to: date })
    }

    componentWillReceiveProps(nextProps) {
        this.state.assigned_to = moment(nextProps.reminder.assigned_to)
        this.state.reminder = nextProps.reminder
        if (nextProps.redirect) {
            this.props.setRedirectFalse()
            this.props.history.push('/' + getPart() + '/reminders/list')
        }
    }

    componentDidMount() {
        this.props.editReminderAction(this.props.match.params.id)
    }

    render() {
        const { reminder, errors, translate, customers, currentLanguage } = this.props

        return (
            <div className="content">
                <div className="container-fluid">
                    <Row>
                        <Col md={12}>
                            <div className="card card-plain">
                                <Row className="card-header"
                                     style={{backgroundImage: "linear-gradient(60deg, rgba(38, 198, 218, 0.6), rgba(0, 172, 193, 0.6))"}}>
                                    <Col xs={8}>
                                        <h4 className="title">{ translate('interface.edit_reminder') }</h4>
                                        <p className="category">
                                            <Button onClick={this.props.history.goBack}
                                                    className="btn btn-sm btn-white static-color">
                                                { translate('interface.back') }
                                            </Button>
                                        </p>
                                    </Col>
                                </Row>

                                <Errors errors={errors} />

                                { reminder &&
                                    <div className="card-content">

                                        <Row>
                                            <Col md={6}>
                                                <div className="form-group label-floating">
                                                    <Label className="control-label">{ translate('interface.assigned_to') }</Label>
                                                    <Datetime
                                                        defaultValue={moment(this.state.assigned_to)}
                                                        onChange={this.handleChangeDate}
                                                        dateFormat="YYYY-MM-DD"
                                                        timeFormat="HH:mm:ss"
                                                        size="md"
                                                        locale={currentLanguage}
                                                    />
                                                </div>
                                            </Col>
                                            <Col md={6}>
                                                <div className="form-group label-floating">
                                                    <Label className="control-label">{ translate('interface.customer') }</Label>
                                                    <select name="content_id" className="form-control"  onChange={this.handleChange.bind(this, 'content_id')}>
                                                        <option value={0}>Choose...</option>
                                                        { (customers && customers.length > 0) && customers.map(function (customer) {
                                                            return (
                                                                <option key={customer.id} value={customer.id}  selected={customer.id === reminder.content_id}  >
                                                                    { customer.firstname } { customer.lastname }
                                                                </option>
                                                            )
                                                        }) }
                                                    </select>
                                                </div>
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={12}>
                                                <div className="form-group label-floating">
                                                    <Label className="control-label">{ translate('interface.note') }</Label>
                                                    <textarea className="form-control"
                                                              rows="3"
                                                              name="note"
                                                              value={reminder.note}
                                                              onChange={this.handleChange.bind(this, 'note')}
                                                    >
                                                </textarea>
                                                </div>
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={6}>
                                                <Button onClick={this.handleSubmit}>{ translate('interface.save') }</Button>
                                            </Col>
                                        </Row>

                                    </div>
                                }
                                <Loader />
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        errors: state.reducer.reminders.errors,
        redirect: state.reducer.redirect,
        customers: state.reducer.reminders.customersList,
        reminder: state.reducer.reminder,
        translate: getTranslate(state.locale),
        currentLanguage: getActiveLanguage(state.locale).code
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateReminderAction: bindActionCreators(updateReminderAction, dispatch),
        setRedirectFalse: bindActionCreators(setRedirectFalse, dispatch),
        editReminderAction: bindActionCreators(editReminderAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReminderEdit)