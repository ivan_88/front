import React, { Component } from "react"
import { Modal, Table, Row, Col, Button } from 'react-bootstrap'
import { getTranslate } from "react-localize-redux"
import { connect } from "react-redux"
import { hideReminderModalAction, doneAction } from "../../actions/actionsReminders"
import { bindActionCreators } from "redux"
import Loader from '../parts/Loader'

class ReminderModal extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { id, reminder, show, translate, hideReminderModalAction, doneAction } = this.props

        if (Object.keys(reminder).length > 0) {
            return (
                <Modal backdrop={true} show={show} aria-labelledby="ModalHeader" onBackdropClick={() => hideReminderModalAction()}>
                    <Modal.Header>
                        <h4>
                            { translate('interface.notes') }
                        </h4>
                    </Modal.Header>
                    <Modal.Body>

                            <div>
                                <Row>
                                    <Col md={3}>
                                        { reminder.content.firstname }  { reminder.content.lastname }
                                    </Col>
                                    <Col md={6}>
                                        { reminder.note }
                                    </Col>
                                    <Col md={3}>
                                        { reminder.assigned_to }
                                    </Col>
                                </Row>

                            </div>

                        <Loader />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button className="btn-info" onClick={() => hideReminderModalAction()}>
                            { translate('interface.close') }
                        </Button>
                        <Button type="button"
                                rel="tooltip"
                                title={ translate('interface.execute') }
                                onClick={() => doneAction(id, { done: 1 })}
                                className="btn-success">
                            { translate('interface.execute') }
                        </Button>
                    </Modal.Footer>
                </Modal>
            )
        } else {
            return null
        }

    }
}
function mapStateToProps(state) {
    return {
        show: state.reducer.reminderModal.show,
        id: state.reducer.reminderModal.id,
        reminder: state.reducer.reminder,
        translate: getTranslate(state.locale)
    }
}

function mapDispatchToProps(dispatch) {
    return {
        hideReminderModalAction: bindActionCreators(hideReminderModalAction, dispatch),
        doneAction: bindActionCreators(doneAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReminderModal)