import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Col, Row } from 'react-bootstrap'
import { hasRole } from '../../utils/checkRoles'
import { getActiveLanguage, getTranslate } from "react-localize-redux/lib/index"
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { getAuthUserAction } from "../../actions/actionsAuth"
import Loader from '../parts/Loader'

class Applications extends Component {

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.props.getAuthUserAction()
    }

    render() {
        const authUser = this.props.authUser
        const translate = this.props.translate

        return (
            <div className="container">
                <Row>
                    <Col md={12} >
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                { authUser.name ? `Hello ${authUser.name}!  Choose application` : null }
                                <Loader />
                            </div>
                            <div className="panel-body">
                                { authUser ?
                                    <Row>
                                        <Col md={4}>
                                            { hasRole(authUser, 'admin') ||  hasRole(authUser, 'manager') ||  hasRole(authUser, 'sales') ||  hasRole(authUser, 'broker') ?
                                                <Link to={`/brokers/customers/list`} className="btn btn-success">
                                                    { translate('interface.brokers') }
                                                </Link>
                                            : null }
                                        </Col>
                                        <Col md={4}>
                                            { hasRole(authUser, 'admin') ||  hasRole(authUser, 'sales') || hasRole(authUser, 'manager') ?
                                                <Link to={`/sales/customers/list`} className="btn btn-success">
                                                    { translate('interface.sales') }
                                                </Link>
                                                : null }
                                        </Col>
                                        <Col md={4}>
                                            { hasRole(authUser, 'admin') || hasRole(authUser, 'partner') ?
                                                <Link to={`/partners/users/list`} className="btn btn-success">
                                                    { translate('interface.network') }
                                                </Link>
                                                : null }
                                        </Col>
                                    </Row>
                                :
                                <Loader />
                                }
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}
function mapStateToProps(state, history) {
    return {
        translates: state.reducer.translates,
        history: history.history,
        translate: getTranslate(state.locale),
        currentLanguage: getActiveLanguage(state.locale).code,
        authUser: state.reducer.authUser
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getAuthUserAction: bindActionCreators(getAuthUserAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Applications)
