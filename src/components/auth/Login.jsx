import React, { Component } from 'react'
import { getTokenAction } from '../../actions/actionsAuth'
import { getActiveLanguage, getTranslate } from "react-localize-redux/lib/index"
import { bindActionCreators } from "redux"
import { connect } from "react-redux"

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            remember: false
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.getTokenAction(this.state)
    }

    handleChange(event) {
        this.state[event.target.name] = event.target.value
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-8 col-md-offset-2">
                        <div className="panel panel-default">
                            <div className="panel-heading">Login</div>
                            <div className="panel-body">
                                <form className="form-horizontal">

                                    <div className="form-group">
                                        <label className="col-md-4 control-label">Email</label>
                                        <div className="col-md-6">
                                            <input id="email"
                                                type="email"
                                                className="form-control"
                                                onChange={this.handleChange}
                                                name="email" required />
                                        </div>
                                    </div>

                                    <div className="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label className="col-md-4 control-label">Password</label>
                                        <div className="col-md-6">
                                            <input id="password"
                                                   type="password"
                                                   className="form-control"
                                                   onChange={this.handleChange}
                                                   name="password" required />
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <div className="col-md-6 col-md-offset-4">
                                            <div className="checkbox">
                                                <label>
                                                    <input type="checkbox"
                                                           name="remember"
                                                           onChange={this.handleChange} /> Remember me
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <div className="col-md-6 col-md-offset-4">
                                            {/*TODO: Reset password*/}
                                            <a href="/password/reset">Forgot password?</a>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <div className="col-md-8 col-md-offset-4">
                                            <button onClick={this.handleSubmit} type="submit" className="btn btn-primary">
                                                Login
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
function mapStateToProps(state, history) {
    return {
        translates: state.reducer.translates,
        history: history.history,
        translate: getTranslate(state.locale),
        currentLanguage: getActiveLanguage(state.locale).code
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getTokenAction: bindActionCreators(getTokenAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
