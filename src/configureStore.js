import { createStore, applyMiddleware } from 'redux'
import reducer from './reducer'
import thunk from 'redux-thunk'

export default function configureStore(initialState) {
    const store = createStore(reducer, initialState,  applyMiddleware(thunk))

    if (module.hot) {
        module.hot.accept('./reducer', () => {
            const nextRootReducer = require('./reducer')
            store.replaceReducer(nextRootReducer)
        })
    }
    return store
}