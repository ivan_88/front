export function hasRole(user, role) {
    if (typeof user.roles ===  'object' && user.roles.length > 0) {
        for (let i in user.roles) {
            if (user.roles[i].name === role) {
                return true
            }
        }
    }
    return false
}

export function canMoveForvard(authUser, type) {
    if (hasRole(authUser, 'admin') || hasRole(authUser, 'manager') || (hasRole(authUser, 'broker') && type === 1)) {
        return true
    }
    return false
}