export function checkPart(part) {
    return part === window.location.href.split('/')[3]
}

export function getPart() {
    return window.location.href.split('/')[3]
}