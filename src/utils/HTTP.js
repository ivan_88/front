import axios from 'axios'
import { backendDomen } from "../config"
import FileDownload from 'react-file-download'

export default class HTTP {
    static get(url, params) {
        return axios({
            method: 'GET',
            url: backendDomen + url,
            params: params,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        }).then(function (value) {
            return value
        }).catch(function (reason) {
            if (reason.response.status === 401) {
                window.location.replace('/')
            }
        })
    }

    static getXLSFile(url, params, filename) {
        return axios({
            method: 'GET',
            url: backendDomen + url,
            responseType: 'arraybuffer',
            params: params,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token'),
                'Content-type': 'application/vnd.ms-excel',
                'Content-Disposition': "attachment"
            }
        }).then(function (value) {
            FileDownload(value.data, filename);
            return value
        }).catch(function (reason) {
            if (reason.response && reason.response.status === 401) {
                window.location.replace('/')
            } else {
                console.error(reason)
            }
        })
    }

    static post(url, data, params) {
        return axios({
            method: 'POST',
            url: backendDomen + url,
            data: data,
            params: params,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        }).then(function (value) {
            return value
        }).catch(function (reason) {
            if (reason.response.status === 401) {
                window.location.replace('/')
            }
            return reason.response
        })
    }
}
