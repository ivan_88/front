import { ACTIONS } from '../actionsConstants'
import HTTP from '../utils/HTTP'

export function changeProjectAction(project) {
    return function(dispatch) {
        dispatch({
            type: ACTIONS.PROJECT.CHANGE,
            payload: project
        })
    }
}

export function changeActiveAction(active) {
    return function(dispatch) {
        dispatch({
            type: ACTIONS.CUSTOMERS.CHANGE_ACTIVE,
            payload: active
        })
    }
}

export function changeTypeAction(type) {
    return function(dispatch) {
        dispatch({
            type: ACTIONS.CUSTOMERS.CHANGE_TYPE,
            payload: type
        })
    }
}

export function sortCustomersAction(order_by, order_direction) {
    return function(dispatch) {
        dispatch({
            type: ACTIONS.CUSTOMERS.SORT,
            payload: {order_by, order_direction}
        })
    }
}

export function getCustomersListAction(params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.CUSTOMERS.GET_LIST.PENDING
        })
        const result = await HTTP.get('/api/customers', params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.CUSTOMERS.GET_LIST.SUCCESS,
                payload: result.data.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.CUSTOMERS.GET_LIST.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function getCustomerAction(id, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.CUSTOMERS.GET_ONE.PENDING
        })
        const result = await HTTP.get('/api/customers/show/' + id, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.CUSTOMERS.GET_ONE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.CUSTOMERS.GET_ONE.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function createCustomerAction(data, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.CUSTOMERS.CREATE.PENDING
        })
        const result = await HTTP.post('/api/customers/store', data, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.CUSTOMERS.CREATE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.CUSTOMERS.CREATE.FAILED,
                payload: result.data
            })
        }
    }
}

export function updateCustomerAction(id, data, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.CUSTOMERS.UPDATE.PENDING
        })
        const result = await HTTP.post('/api/customers/update/' + id, data, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.CUSTOMERS.UPDATE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.CUSTOMERS.UPDATE.FAILED,
                payload: result.data
            })
        }
    }
}

export function moveCustomerAction(params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.CUSTOMERS.MOVE.PENDING
        })
        const result = await HTTP.get('/api/customers/move/' + params.id, { action: params.action })
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.CUSTOMERS.MOVE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.CUSTOMERS.MOVE.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function activeCustomerAction(id, data) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.CUSTOMERS.ACTIVE.PENDING
        })
        const result = await HTTP.post('/api/customers/active/' + id, data)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.CUSTOMERS.ACTIVE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.CUSTOMERS.ACTIVE.FAILED,
                payload: result.data.errors
            })
        }
    }
}

export function showCustomersActiveModal(params) {
    return function (dispatch) {
        dispatch({
            type: ACTIONS.CUSTOMERS.ACTIVE_MODAL.SHOW,
            payload: params
        })
    }
}

export function hideCustomersActiveModal() {
    return function (dispatch) {
        dispatch({
            type: ACTIONS.CUSTOMERS.ACTIVE_MODAL.HIDE
        })
    }
}

export function addNoteAction(id, data) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.CUSTOMERS.ADD_NOTE.PENDING
        })
        const result = await HTTP.post('/api/customers/note/' + id, data)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.CUSTOMERS.ADD_NOTE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.CUSTOMERS.ADD_NOTE.FAILED,
                payload: result.data.errors
            })
        }
    }
}

export function showAddNoteModal(params) {
    return function (dispatch) {
        dispatch({
            type: ACTIONS.CUSTOMERS.NOTE_MODAL.SHOW,
            payload: params
        })
    }
}

export function hideAddNoteModal() {
    return function (dispatch) {
        dispatch({
            type: ACTIONS.CUSTOMERS.NOTE_MODAL.HIDE
        })
    }
}

export function showNotesListModalAction(id) {
    return function (dispatch) {
        dispatch({
            type: ACTIONS.CUSTOMERS.NOTES_LIST_MODAL.SHOW,
            payload: id
        })
    }
}

export function hideNotesListModalAction() {
    return function (dispatch) {
        dispatch({
            type: ACTIONS.CUSTOMERS.NOTES_LIST_MODAL.HIDE
        })
    }
}

export function getNotesListAction(params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.CUSTOMERS.GET_NOTES_LIST.PENDING
        })
        const result = await HTTP.get('/api/customers/notes/' + params.id, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.CUSTOMERS.GET_NOTES_LIST.SUCCESS,
                payload: result.data.notes
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.CUSTOMERS.GET_NOTES_LIST.FAILED,
                payload: result.errors
            })
        }
    }
}
