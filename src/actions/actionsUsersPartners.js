import { ACTIONS } from '../actionsConstants'
import HTTP from '../utils/HTTP'

export function getUsersPartnersListAction(params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.USERS_PARTNERS.GET_LIST.PENDING
        })
        const result = await HTTP.get('/api/partners', params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.USERS_PARTNERS.GET_LIST.SUCCESS,
                payload: result.data.users
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.USERS_PARTNERS.GET_LIST.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function getUsersPartnersCountAction(params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.USERS_PARTNERS.GET_COUNT.PENDING
        })
        const result = await HTTP.get('/api/partners/count', params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.USERS_PARTNERS.GET_COUNT.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.USERS_PARTNERS.GET_COUNT.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function getUserPartnerOneAction(id, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.USERS_PARTNERS.GET_ONE.PENDING
        })
        const result = await HTTP.get('/api/partners/show/' + id, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.USERS_PARTNERS.GET_ONE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.USERS_PARTNERS.GET_ONE.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function createUserPartnerAction(data, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.USERS_PARTNERS.CREATE.PENDING
        })
        const result = await HTTP.post('/api/partners/store', data, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.USERS_PARTNERS.CREATE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.USERS_PARTNERS.CREATE.FAILED,
                payload: result.data
            })
        }
    }
}

export function updateUserPartnerAction(id, data, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.USERS_PARTNERS.UPDATE.PENDING
        })
        const result = await HTTP.post('/api/partners/update/' + id, data, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.USERS_PARTNERS.UPDATE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.USERS_PARTNERS.UPDATE.FAILED,
                payload: result.data
            })
        }
    }
}

export function deleteUserPartnerAction(id, data) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.USERS_PARTNERS.DELETE.PENDING
        })
        const result = await HTTP.get('/api/partners/delete/' + id, data)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.USERS_PARTNERS.DELETE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.USERS_PARTNERS.DELETE.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}