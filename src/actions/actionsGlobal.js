import { ACTIONS } from '../actionsConstants'
import HTTP from '../utils/HTTP'

export function initAction() {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.INIT.PENDING
        })
        const result = await HTTP.get('/api/init')
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.INIT.SUCCESS,
                payload: result.data
            })
        } else {
            dispatch({
                type: ACTIONS.INIT.FAILED,
                payload: { errors: ['Failed to GET /api/init'] }
            })
        }
    }
}

export function getTranslatesAction() {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.TRANSLATES.PENDING
        })
        const result = await HTTP.get('/api/translates')
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.TRANSLATES.SUCCESS,
                payload: result.data
            })
        } else {
            dispatch({
                type: ACTIONS.TRANSLATES.FAILED,
                payload: { errors: ['Failed to GET /api/translates'] }
            })
        }
    }
}

export function changeLanguageAction(lang) {
    return function(dispatch) {
        dispatch({
            type: ACTIONS.LANGUAGE.CHANGE,
            payload: lang
        })
    }
}

export function setRedirectFalse() {
    return function (dispatch) {
        dispatch({
            type: ACTIONS.REDIRECT_FALSE
        })
    }
}