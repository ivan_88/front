import { ACTIONS } from '../actionsConstants'
import HTTP from '../utils/HTTP'

export function getInterestsListAction(params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.INTERESTS.GET_LIST.PENDING
        })
        const result = await HTTP.get('/api/interests', params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.INTERESTS.GET_LIST.SUCCESS,
                payload: result.data.interests
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.INTERESTS.GET_LIST.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function getInterestAction(id, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.INTERESTS.GET_ONE.PENDING
        })
        const result = await HTTP.get('/api/interests/show/' + id, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.INTERESTS.GET_ONE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.INTERESTS.GET_ONE.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function createInterestAction(data, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.INTERESTS.CREATE.PENDING
        })
        const result = await HTTP.post('/api/interests/store', data, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.INTERESTS.CREATE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.INTERESTS.CREATE.FAILED,
                payload: result.data
            })
        }
    }
}

export function updateInterestAction(id, data, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.INTERESTS.UPDATE.PENDING
        })
        const result = await HTTP.post('/api/interests/update/' + id, data, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.INTERESTS.UPDATE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.INTERESTS.UPDATE.FAILED,
                payload: result.data
            })
        }
    }
}

export function deleteInterestAction(id, data) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.INTERESTS.DELETE.PENDING
        })
        const result = await HTTP.get('/api/interests/delete/' + id, data)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.INTERESTS.DELETE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.INTERESTS.DELETE.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}