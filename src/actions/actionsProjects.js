import { ACTIONS } from '../actionsConstants'
import HTTP from '../utils/HTTP'

export function getProjectsListAction(params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.PROJECTS.GET_LIST.PENDING
        })
        const result = await HTTP.get('/api/projects', params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.PROJECTS.GET_LIST.SUCCESS,
                payload: result.data.projects
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.PROJECTS.GET_LIST.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function getProjectsAction(id, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.PROJECTS.GET_ONE.PENDING
        })
        const result = await HTTP.get('/api/projects/show/' + id, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.PROJECTS.GET_ONE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.PROJECTS.GET_ONE.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function createProjectsAction(data, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.PROJECTS.CREATE.PENDING
        })
        const result = await HTTP.post('/api/projects/store', data, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.PROJECTS.CREATE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.PROJECTS.CREATE.FAILED,
                payload: result.data
            })
        }
    }
}

export function updateProjectsAction(id, data, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.PROJECTS.UPDATE.PENDING
        })
        const result = await HTTP.post('/api/projects/update/' + id, data, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.PROJECTS.UPDATE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.PROJECTS.UPDATE.FAILED,
                payload: result.data
            })
        }
    }
}

export function deleteProjectsAction(id, data) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.PROJECTS.DELETE.PENDING
        })
        const result = await HTTP.get('/api/projects/delete/' + id, data)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.PROJECTS.DELETE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.PROJECTS.DELETE.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}