import { ACTIONS } from '../actionsConstants'
import HTTP from '../utils/HTTP'

// TODO: Саги

export function getLogsByCustomerAction(params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.LOGS.GET_LIST_BY_CUSTOMER.PENDING
        })
        const result = await HTTP.get('/api/logs/by_customer/' + params.id, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.LOGS.GET_LIST_BY_CUSTOMER.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.LOGS.GET_LIST_BY_CUSTOMER.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function getLogsByUserAction(params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.LOGS.GET_LIST_BY_USER.PENDING
        })
        const result = await HTTP.get('/api/logs/by_user/' + params.id, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.LOGS.GET_LIST_BY_USER.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.LOGS.GET_LIST_BY_USER.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function getAllLogsAction(params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.LOGS.GET_ALL.PENDING
        })
        const result = await HTTP.get('/api/logs', params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.LOGS.GET_ALL.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.LOGS.GET_ALL.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}


export function getLogsByUserExcelAction(id, filename = 'log.xls') {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.LOGS.GET_LIST_BY_USER_EXCEL.PENDING
        })
        const result = await HTTP.getXLSFile('/api/logs/by_user/' + id, { excel: 1 }, filename)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.LOGS.GET_LIST_BY_USER_EXCEL.SUCCESS
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.LOGS.GET_LIST_BY_USER.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}


export function getLogsByCustomerExcelAction(id, filename = 'log.xls') {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.LOGS.GET_LIST_BY_CUSTOMER_EXCEL.PENDING
        })
        const result = await HTTP.getXLSFile('/api/logs/by_customer/' + id, { excel: 1 }, filename)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.LOGS.GET_LIST_BY_CUSTOMER_EXCEL.SUCCESS
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.LOGS.GET_LIST_BY_CUSTOMER_EXCEL.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function getAllLogsExcelAction(params, filename = 'log.xls') {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.LOGS.GET_LIST_BY_CUSTOMER_EXCEL.PENDING
        })
        const result = await HTTP.getXLSFile('/api/logs', params, filename)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.LOGS.GET_LIST_BY_CUSTOMER_EXCEL.SUCCESS
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.LOGS.GET_LIST_BY_CUSTOMER_EXCEL.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}