import { ACTIONS } from '../actionsConstants'
import HTTP from '../utils/HTTP'

export function getUsersListAction(params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.USERS.GET_LIST.PENDING
        })
        const result = await HTTP.get('/api/users', params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.USERS.GET_LIST.SUCCESS,
                payload: result.data.users
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.USERS.GET_LIST.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function getUserAction(id, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.USERS.GET_ONE.PENDING
        })
        const result = await HTTP.get('/api/users/show/' + id, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.USERS.GET_ONE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.USERS.GET_ONE.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function createUserAction(data, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.USERS.CREATE.PENDING
        })
        const result = await HTTP.post('/api/users/store', data, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.USERS.CREATE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.USERS.CREATE.FAILED,
                payload: result.data
            })
        }
    }
}

export function updateUserAction(id, data, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.USERS.UPDATE.PENDING
        })
        const result = await HTTP.post('/api/users/update/' + id, data, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.USERS.UPDATE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.USERS.UPDATE.FAILED,
                payload: result.data
            })
        }
    }
}

export function deleteUserAction(id, data) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.USERS.DELETE.PENDING
        })
        const result = await HTTP.get('/api/users/delete/' + id, data)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.USERS.DELETE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.USERS.DELETE.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function changePasswordAction(data, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.USERS.CHANGE_PASSWORD.PENDING
        })
        const result = await HTTP.post('/api/users/password', data, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.USERS.CHANGE_PASSWORD.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.USERS.CHANGE_PASSWORD.FAILED,
                payload: result.data
            })
        }
    }
}