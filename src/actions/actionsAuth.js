import { ACTIONS } from '../actionsConstants'
import HTTP from '../utils/HTTP'
import { clientId, secret } from "../config"

export function getTokenAction(params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.AUTH.LOGIN.PENDING
        })
        const result = await HTTP.post('/api/oauth/token', {
            client_id: clientId,
            client_secret: secret,
            grant_type: 'password',
            username: params.email,
            password: params.password,
            scope: '*'
        })
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.AUTH.LOGIN.SUCCESS
            })
            localStorage.setItem('token', result.data.access_token)
            window.location.replace('/applications')
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.AUTH.LOGIN.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function getAuthUserAction() {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.AUTH.GET_USER.PENDING
        })
        const result = await HTTP.get('/api/auth_user')
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.AUTH.GET_USER.SUCCESS,
                payload: result.data.authUser
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.AUTH.GET_USER.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}