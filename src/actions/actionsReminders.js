import { ACTIONS } from '../actionsConstants'
import HTTP from '../utils/HTTP'

export function getRemindersListAction(params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.REMINDERS.GET_LIST.PENDING
        })
        const result = await HTTP.get('/api/reminders', params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.REMINDERS.GET_LIST.SUCCESS,
                payload: result.data.reminders
            })
        } else if (result.status !== 200) {
            dispatch({
                type: ACTIONS.REMINDERS.GET_LIST.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function getTodayRemindersListAction(params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.REMINDERS.GET_TODAY_LIST.PENDING
        })
        const result = await HTTP.get('/api/reminders/today', params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.REMINDERS.GET_TODAY_LIST.SUCCESS,
                payload: result.data
            })
        } else if (result.status !== 200) {
            dispatch({
                type: ACTIONS.REMINDERS.GET_TODAY_LIST.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function getCustomersListAction(params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.REMINDERS.GET_LIST_CUSTOMERS.PENDING
        })
        const result = await HTTP.get('/api/reminders/create', params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.REMINDERS.GET_LIST_CUSTOMERS.SUCCESS,
                payload: result.data.customers
            })
        } else if (result.status !== 200) {
            dispatch({
                type: ACTIONS.REMINDERS.GET_LIST_CUSTOMERS.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function getReminderAction(id, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.REMINDERS.GET_ONE.PENDING
        })
        const result = await HTTP.get('/api/reminders/show/' + id, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.REMINDERS.GET_ONE.SUCCESS,
                payload: result.data
            })
        } else if (result.status !== 200) {
            dispatch({
                type: ACTIONS.REMINDERS.GET_ONE.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function editReminderAction(id, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.REMINDERS.EDIT.PENDING
        })
        const result = await HTTP.get('/api/reminders/edit/' + id, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.REMINDERS.EDIT.SUCCESS,
                payload: result.data
            })
        } else if (result.status !== 200) {
            dispatch({
                type: ACTIONS.REMINDERS.EDIT.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function createReminderAction(data, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.REMINDERS.CREATE.PENDING
        })
        const result = await HTTP.post('/api/reminders/store', data, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.REMINDERS.CREATE.SUCCESS,
                payload: result.data
            })
        } else if (result.status !== 200) {
            dispatch({
                type: ACTIONS.REMINDERS.CREATE.FAILED,
                payload: result.data
            })
        }
    }
}

export function updateReminderAction(id, data, params = {}) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.REMINDERS.UPDATE.PENDING
        })
        const result = await HTTP.post('/api/reminders/update/' + id, data, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.REMINDERS.UPDATE.SUCCESS,
                payload: result.data
            })
        } else if (result.status !== 200) {
            dispatch({
                type: ACTIONS.REMINDERS.UPDATE.FAILED,
                payload: result.data
            })
        }
    }
}

export function deleteReminderAction(id, data) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.REMINDERS.DELETE.PENDING
        })
        const result = await HTTP.get('/api/reminders/delete/' + id, data)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.REMINDERS.DELETE.SUCCESS,
                payload: result.data
            })
        } else if (result.status !== 200) {
            dispatch({
                type: ACTIONS.REMINDERS.DELETE.FAILED,
                payload: { errors: result.errors }
            })
        }
    }
}

export function changeExecutedAction(executed) {
    return function(dispatch) {
        dispatch({
            type: ACTIONS.REMINDERS.CHANGE_EXECUTED,
            payload: executed
        })
    }
}

export function changeTodayAction(today) {
    return function(dispatch) {
        dispatch({
            type: ACTIONS.REMINDERS.CHANGE_TODAY,
            payload: today
        })
    }
}

export function doneAction(id, params) {
    return async function(dispatch) {
        dispatch({
            type: ACTIONS.REMINDERS.DONE.PENDING
        })
        const result = await HTTP.get('/api/reminders/done/' + id, params)
        if (result.status === 200) {
            dispatch({
                type: ACTIONS.REMINDERS.DONE.SUCCESS,
                payload: result.data
            })
        } else if(result.status !== 200) {
            dispatch({
                type: ACTIONS.REMINDERS.DONE.FAILED,
                payload: result.data.errors
            })
        }
    }
}

export function showReminderModalAction(id) {
    return function (dispatch) {
        dispatch({
            type: ACTIONS.REMINDERS.MODAL.SHOW,
            payload: id
        })
    }
}

export function hideReminderModalAction() {
    return function (dispatch) {
        dispatch({
            type: ACTIONS.REMINDERS.MODAL.HIDE
        })
    }
}